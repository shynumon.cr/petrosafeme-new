<?php
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
include('connection.php');
date_default_timezone_set("Asia/Dubai");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Petrosafe | Committed to gas and fire safety</title>
<!-- Stylesheets -->
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link href="../css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->


</head>

<body>

<style>
.principles { display: none;}


.bok-his-set-main { background: #F2F2F2; border-radius: 3px; padding: 10px 15px 0px 15px !important; margin-top: 20px;}
.booking-his-set-box { padding-bottom: 10px; padding-top: 10px;}
.booking-his-set { padding: 0px 20px 0px 20px;}
.bok-mid-set { border-left: 2px solid #FFF; border-right: 2px solid #FFF; }
.total-set p { color: #1b5880; font-weight: bold;}
.booking-cont-set-main h4 { font-size: 18px; line-height: 20px;}

.book-sum-left span { float: right;}

.success-set { width: 80px; height: 80px; border-radius: 50%; overflow: hidden; margin:0 auto 20px;}


@media all and (max-width: 991px) and (min-width: 320px) {

.bok-his-set-main { padding: 0px 0px 0px 0px !important;}

}
	
</style>
<?php
// $outlet = 'd4515e78-da84-41a2-b9c9-4a0e2e9c8876';
$outlet = '2c36ce58-2c88-4aed-9b29-acfb53bde3c9';
// $apikey = 'NjMyMzA3NzQtNWU5ZC00MzkwLTk4MzctYTY5ZjljNDcwMDAwOjk5YmU1OWQ3LTQ0Y2UtNDc3NC1hNmNjLTE2NmEyMjcxM2IxNg==';
$apikey = 'MmQxZmUyNjMtNmRmMC00MGNkLTkyZmUtZWQ1ODBiOGM4ODRlOjM2NmE2NGQxLTkzMGYtNDJhMC1iOWFhLTg5OGNhMWE0ZWNhYg==';
$pay_stat='';
$payment_stat=0;
if (isset($_REQUEST['ref'])) 
{ 
	$orderReference = $_REQUEST['ref'];

	try {
		$idData = identify($apikey);
		$idData = json_decode($idData);
		if (isset($idData->access_token)) { 
			$token = $idData->access_token;
			$statusData = orderSstatus($orderReference, $token, $outlet);
			$res=json_decode($statusData, true);
			$arrayResponse = (array) $statusData;
			//echo '<pre>'; print_r($res['_embedded']['payment'][0]['savedCard']);
			$tok = json_encode($res['_embedded']['payment'][0]['savedCard']);
			//echo $tok;
			// echo '<pre>';
			// print_r($res);
			// exit();
			$pay_stat=$res['_embedded']['payment'][0]['state'];
			$pay_amt=$res['amount']['value'];
			echo $pay_amt=($pay_amt/100);
			exit();
			$name = $res['billingAddress']['firstName'];
			$addrs = $res['billingAddress']['address1'];
			$city = $res['billingAddress']['city'];
			$merchant_order = $res['merchantOrderReference'];
			if(strtolower($pay_stat)=='captured'){$payment_stat=1;}
			//$payrecord_id=$_SESSION['payrecord_id'];
			//DB insert
			//$payrecord_id=$_SESSION['payrecord_id'];
			if(strtolower($pay_stat)=='captured')
			{
				$mobile = $res['merchantDefinedData']['mobile'];
				$message = "Thank you for the payment of AED ".$pay_amt." against the order with Ref. ".$merchant_order;
				if($mobile != "")
				{
					$message="Dear ".$name.",\n".$message."\n Team Petrosafe";
					//send_sms($mobile, $message);
				}
			}
			
			
			
			 $curdatetime=date("Y-m-d H:i:s");
			 $custid = $_GET['id'];
			 if($custid>0)
			 {
				 $paymentidnew = $res['merchantDefinedData']['paymentid'];
				 //$updatesql = "update payment set pay_status ='".$payment_stat."', pay_status_text = '".$pay_stat."', pay_order_ref = '".$merchant_order."', pay_reference = '".$orderReference."', paid_amount = '".$pay_amt."', pay_completed_time = '".$curdatetime."' where pay_id = ".$paymentidnew;
				 
				 //$paymentidquery = mysqli_query($con,$updatesql);
				
				//$sql = "INSERT INTO payment (pay_cust_id,pay_cust_name,pay_status,pay_status_text,pay_order_ref,pay_reference,paid_amount,pay_init_time,pay_completed_time)VALUES ('".$custid."','".$name."','".$payment_stat."','".$pay_stat."','".$merchant_order."','".$orderReference."','".$pay_amt."','".$curdatetime."','".$curdatetime."')";
				//$resultImg = mysqli_query($con,$sql);
				//$payrecord_id= mysqli_insert_id($con);
				 
				 if($custid >0 && $pay_amt >0)
				 {
					 // if(strtolower($pay_stat)=='captured')
					// {
						// $url = 'https://erp.hwgas.ae/utility/payments';  
						// $data = array(
							// 'user_id' => 14,
							// 'customer_id' => $custid,
							// 'payment_method' => 9,
							// 'notes' => 'Payment reference number '.$orderReference.' with order id '.$merchant_order,
							// 'amount' => $pay_amt,
							// 'date' => date('Y-m-d'),
						// );
						// $payload = json_encode(array("params" => $data));	
						
						// $ch = curl_init();
						// curl_setopt($ch, CURLOPT_URL, $url);
						// curl_setopt($ch, CURLOPT_POST, true);
						// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
						// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						// curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
						// $pay_update_op = json_decode(curl_exec($ch)); 
						
						// $errlat = curl_error($ch);
						
						// curl_close($ch);
					// }
				 }
				 else{
				 }
			 }
			
			//DB ENDS
		   
		   
		}
	} catch (Exception $e) {

	  echo($e->getMessage());

	}
}

function send_sms($mobile_number, $message)
{
	$num = substr($mobile_number, -9);
	
	$sms_url = 'http://sms.azinova.in/WebServiceSMS.aspx?User=happyway&passwd=hwgas@123&mobilenumber=971'.$num.'&message='.urlencode($message).'&sid=BULKSMS&mtype=N';
	$sms_url = str_replace(" ", '%20', $sms_url);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $sms_url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	$res = curl_exec($ch);
	curl_close ($ch);
}
		
function identify($apikey) { 

	//$idUrl = "https://identity-uat.ngenius-payments.com/auth/realms/ni/protocol/openid-connect/token";
	// $idUrl = "https://identity.ngenius-payments.com/auth/realms/ni/protocol/openid-connect/token";
	$idUrl = "https://identity.ngenius-payments.com/auth/realms/networkinternational/protocol/openid-connect/token";
	$idHead = array("Authorization: Basic ".$apikey, "Content-Type: application/x-www-form-urlencoded");
	$idPost = http_build_query(array('grant_type' => 'client_credentials'));
	$idOutput = invokeCurlRequest("POST", $idUrl, $idHead, $idPost);
	return $idOutput;
}

                    function orderSstatus($orderReference, $token, $outlet) {        
                        // construct order object JSON
                        $payPost = '';
                        // $statusUrl = "https://api-gateway-uat.ngenius-payments.com/transactions/outlets/".$outlet."/orders/".$orderReference;
                        $statusUrl = "https://api-gateway.ngenius-payments.com/transactions/outlets/".$outlet."/orders/".$orderReference;
                        $payHead = array("Authorization: Bearer ".$token, "Content-Type: application/vnd.ni-payment.v2+json", "Accept: application/vnd.ni-payment.v2+json");
                       // $payPost = json_encode($ord); 

                        $payOutput = invokeCurlRequest("GET", $statusUrl, $payHead, $payPost);
                        return $payOutput;
                    }

                    function oneStage($orderReference, $token, $outlet) {        
                        // construct order object JSON
                        $payPost = '';
                        $statusUrl = "https://api-gateway.sandbox.ngenius-payments.com/transactions/outlets/".$outlet."/payment/saved-card";
                        $payHead = array("Authorization: Bearer ".$token, "Content-Type: application/vnd.ni-payment.v2+json", "Accept: application/vnd.ni-payment.v2+json");
                        $payPost = json_encode($ord); 

                        $payOutput = invokeCurlRequest("PUT", $statusUrl, $payHead, $payPost);
                        return $payOutput;
                    }

                    function invokeCurlRequest($type, $url, $headers, $post) {
                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);       

                        if ($type == "POST" || $type == "PUT") {
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                            if ($type == "PUT") {
                              curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                            }
                        }

                        $server_output = curl_exec ($ch);
                       // $res = json_decode($server_output);
                        return $server_output;

                    }
?>




<header class="nps-header">
   <div class="auto-container">
       <div class="row clearfix">
           <div class="col-md-12 col-sm-12 no-left-right-padding">
           
                <div class="col-lg-3 col-md-3 col-sm-12 pl-0 pr-0">
                     <div class="logo"><a href="payment"><img src="../images/logo.png" alt=""></a></div>
                     <div class="mob-menu-icon"><img src="../images/menu.png"></div><!--logo end-->
                     <div class="clear"></div> 
                </div>
                
                <div class="col-lg-9 col-md-9 col-sm-9 nps-menu menu pl-0 pr-0">
                     <nav id="primary_nav_wrap">
                          <ul>                                   
                              
                              <li class="nps-icon2"><a href="payment"> &nbsp;&nbsp; Online Payment</a></li>
                              <li class="nps-icon1"><a href="utility/self-reading">Self Reading</a></li>
                              <li class="nps-icon3"><a href="../register.php">Register</a></li>
                              
                              <div class="clear"></div>
                          </ul>
                          <div class="clear"></div>   
                     </nav>
                    <div class="clear"></div>
                </div>
           
           
           </div>
       </div>
   </div>
</header>



<div class="page-wrapper">

    
    <!--industry-->
    <section class="industry-section nps-pb-100">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                
                
                
         <div class="page-info">
        	<div class="auto-container"><br>
<br>

            	<div class="row clearfix">
						
						<?php
						if(strtolower($pay_stat)=='captured')
						{
						?>
                      
                      <div class="success-set"><img src="../images/success.gif" alt=""></div>
                
                
                     <h2 class="text-center" style="line-height: 34px;"><strong>THANK YOU</strong><br>Your payment has been successful!</h2>
					 
					 <?php
						} else {
					 ?>
					 <h2 class="text-center" style="line-height: 34px;">Your payment has been failed!</h2>
					 <?php
						}
					 ?>
 
 
                   <div class="col-lg-12 col-md-12 col-sm-12">
 

                  <div class="col-lg-12 col-md-12 col-sm-12  bok-his-set-main no-left-right-padding">
                
                  <div class="col-lg-6 col-md-12 col-sm-12 booking-his-set-box">
                                               
                     <h4><strong>Personal Details</strong></h4>
                     
                      <div class="col-lg-12 col-md-12 col-sm-12 booking-his-set no-left-right-padding">
                           <div class="row ml-0 mr-0">
                                <div class="col-6 col-lg-4 col-md-6 col-sm-6 col-xs-6 book-sum-left no-right-padding"><p>Name <span>:</span></p></div>
                                <div class="col-8 col-lg-8 col-md-6 col-sm-6 col-xs-6 book-sum-right pr-0"><p><?php echo $name; ?></p></div>
                           </div>
                      </div>
            
            
                      <!--<div class="col-lg-12 col-md-12 col-sm-12 booking-his-set no-left-right-padding">
                           <div class="row ml-0 mr-0">
                                <div class="col-6 col-lg-4 col-md-6 col-sm-6 col-xs-6 book-sum-left no-right-padding"><p>Email ID <span>:</span></p></div>
                                <div class="col-8 col-lg-6 col-md-6 col-sm-6 col-xs-6 book-sum-right pl-0 pr-0"><p>tom@gmail.com</p></div>
                           </div>
                      </div>-->
                      
                      
                      <!--<div class="col-lg-12 col-md-12 col-sm-12 booking-his-set no-left-right-padding">
                           <div class="row ml-0 mr-0">
                                <div class="col-6 col-lg-4 col-md-6 col-sm-6 col-xs-6 book-sum-left no-right-padding"><p>Contact Number <span>:</span></p></div>
                                <div class="col-8 col-lg-6 col-md-6 col-sm-6 col-xs-6 book-sum-right pl-0 pr-0"><p>0987654321</p></div>
                           </div>
                      </div>-->
                      
                     
                      
                      
                      
                      <div class="col-lg-12 col-md-12 col-sm-12 booking-his-set no-left-right-padding">
                           <div class="row ml-0 mr-0">
                                <div class="col-6 col-lg-4 col-md-6 col-sm-6 col-xs-6 book-sum-left no-right-padding"><p>Address <span>:</span></p></div>
                                <div class="col-8 col-lg-8 col-md-6 col-sm-6 col-xs-6 book-sum-right pr-0"><p><?php echo $addrs; ?></p></div>
                           </div>
                      </div>
                      
                      
                       <div class="col-lg-12 col-md-12 col-sm-12 booking-his-set no-left-right-padding">
                           <div class="row ml-0 mr-0">
                                <div class="col-6 col-lg-4 col-md-6 col-sm-6 col-xs-6 book-sum-left no-right-padding"><p>City  <span>:</span></p></div>
                                <div class="col-8 col-lg-8 col-md-6 col-sm-6 col-xs-6 book-sum-right pr-0"><p><?php echo $city; ?></p></div>
                           </div>
                      </div>
            
            
                      
            
                </div>
                
                
                
                
                  <div class="col-lg-6 col-md-12 col-sm-12 booking-his-set-box">
                                               
                     <h4><strong>Bill Details</strong></h4>
                     
                     
                
                     
                      <!--<div class="col-lg-12 col-md-12 col-sm-12 booking-his-set no-left-right-padding">
                           <div class="row ml-0 mr-0">
                                <div class="col-6 col-lg-4 col-md-6 col-sm-6 col-xs-6 book-sum-left no-right-padding"><p>Last reading <span>:</span></p></div>
                                <div class="col-8 col-lg-6 col-md-6 col-sm-6 col-xs-6 book-sum-right pl-0 pr-0"><p>0000568</p></div>
                           </div>
                      </div>
                      
                      
                      <div class="col-lg-12 col-md-12 col-sm-12 booking-his-set no-left-right-padding">
                           <div class="row ml-0 mr-0">
                                <div class="col-6 col-lg-4 col-md-6 col-sm-6 col-xs-6 book-sum-left no-right-padding"><p>Last reading amount  <span>:</span></p></div>
                                <div class="col-8 col-lg-6 col-md-6 col-sm-6 col-xs-6 book-sum-right pl-0 pr-0"><p>AED 150</p></div>
                           </div>
                      </div>-->
                      
                      
                      <!--<div class="col-lg-12 col-md-12 col-sm-12 booking-his-set no-left-right-padding">
                           <div class="row ml-0 mr-0">
                                <div class="col-6 col-lg-4 col-md-6 col-sm-6 col-xs-6 book-sum-left no-right-padding"><p>Contract ID <span>:</span></p></div>
                                <div class="col-8 col-lg-6 col-md-6 col-sm-6 col-xs-6 book-sum-right pl-0 pr-0"><p>PT 6584</p></div>
                           </div>
                      </div>-->
                      
                      <div class="col-lg-12 col-md-12 col-sm-12 booking-his-set no-left-right-padding">
                           <div class="row ml-0 mr-0">
                                <div class="col-6 col-lg-4 col-md-6 col-sm-6 col-xs-6 book-sum-left no-right-padding"><p>Order reference  <span>:</span></p></div>
                                <div class="col-8 col-lg-8 col-md-6 col-sm-6 col-xs-6 book-sum-right pr-0"><p><?php echo $merchant_order; ?></p></div>
                           </div>
                      </div>
                      
                      
                      <div class="col-lg-12 col-md-12 col-sm-12 booking-his-set no-left-right-padding">
                           <div class="row ml-0 mr-0">
                                <div class="col-6 col-lg-4 col-md-6 col-sm-6 col-xs-6 book-sum-left no-right-padding"><p>Paid amount  <span>:</span></p></div>
                                <div class="col-8 col-lg-8 col-md-6 col-sm-6 col-xs-6 book-sum-right pr-0"><p><strong>AED <?php echo $pay_amt; ?></strong></p></div>
                           </div>
                      </div>
            
                </div>
            
             
                  </div>  
                  
                  
                  </div>
                
              </div>
            </div>
        </div>
                
                
            </div>
        </div>
    </section>
    
    <!--services-style-three-->
    <section class="services-style-three">
    	<div class="auto-container">
        	<div class="row clearfix">
            
            
            







            
            	
                
                
                
                
                
                
            </div>
        </div>
    </section>
    
        




    
</div>



<footer class="nps-footer">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-md-12 col-sm-12 no-left-right-padding">
            
              <div class="col-md-5 col-sm-12 nps-foot-left no-left-right-padding">
                <p><strong>Member of HappyWay Group</strong><br>Petrosafe © 2020 All Rights Reserved.</p>
              </div>
                 
                 <div class="col-md-2 col-sm-12 nps-foot-mid no-left-right-padding"><img src="../images/happy-logo.png"></div>
              <div class="col-md-5 col-sm-12 nps-foot-right no-left-right-padding">
                      <p><strong>Phone : +971 4 267 7294<br>Email  : utility@petrosafeme.com</strong></p> 
              </div>
            
            </div>
        </div>
    </div>
</footer>



<script src="../js/jquery.js"></script> 



<!--End Google Map APi-->
<script>
$( document ).ready(function() {
	
	
	$('.mob-menu-icon') .click(function(){
  $('.menu').toggle(1000);
  });
  
  
  $('.mob-menu-hide') .click(function(){
  $('.menu').hide(1000);
  });
  
  });
	</script>



</body>
</html>