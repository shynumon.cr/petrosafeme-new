<?php
$url = 'https://utility.happywaygas.com/utility/payments';  
$data = array(
	'user_id' => 14,
	'customer_id' => 2139,
	'payment_method' => 9,
	'notes' => 'Payment reference number test with order id ',
	'amount' => 1,
	'date' => date('Y-m-d'),
);
$payload = json_encode(array("params" => $data));	

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
$pay_update_op = json_decode(curl_exec($ch)); 
echo '<pre>';
print_r($pay_update_op);
$errlat = curl_error($ch);

curl_close($ch);


?>