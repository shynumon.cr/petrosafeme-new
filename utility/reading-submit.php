<?php
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
include('connection.php');
date_default_timezone_set("Asia/Dubai");

$insertmsg=0;
if(isset($_POST['reading_submit']))
  {
	  $reading=$_POST['txt_reading'];
	  $meterid=$_POST['txt_meter_id'];
	  $cust_id=$_POST['hid_custid'];
	  $cust_name=$_POST['hid_cust_name'];
	  $cust_email=$_POST['txt_email'];
	  $cust_buildname=$_POST['txt_build_name'];
	  $cust_flatname=$_POST['txt_flat_name'];
	  $cust_mob=$_POST['txt_mob'];
	  $previous_reading_date=$_POST['previous_reading_date'];
	  //echo $reading.",".$cust_id;
	  
	  $target_dir = "uploads/readings/";
	  $target_file = $_POST['hid_image_url'];
	  //move_uploaded_file($_FILES["meter_image"]["tmp_name"], $target_file);
	  
	  $curdatetime=date("Y-m-d H:i:s");
	  //$sql = "INSERT INTO meter_reading_updates (upd_cust_id,upd_cust_name,upd_reading,upd_image,upd_added_datetime)VALUES ('".$cust_id."','".$cust_name."','".$reading."','".$target_file."','".$curdatetime."')";
	  $sql = "INSERT INTO meter_reading_updates (upd_cust_id,upd_cust_name,upd_cust_email,upd_cust_mob,upd_cust_building_name,upd_cust_flat_name,previous_reading_date,upd_reading,upd_image,meter_number,upd_added_datetime)VALUES ('".$cust_id."','".$cust_name."','".$cust_email."','".$cust_mob."','".$cust_buildname."','".$cust_flatname."','".$previous_reading_date."','".$reading."','".$target_file."','".$meterid."','".$curdatetime."')";
	  $rslt = mysqli_query($con,$sql);
	  $insertmsg=1;
  }


?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Petrosafe | Committed to gas and fire safety</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!--Favicon-->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->


</head>

<body>

<style>
.principles { display: none;}


.bok-his-set-main { background: #F2F2F2; border-radius: 3px; padding: 10px 15px 0px 15px !important; margin-top: 20px;}
.booking-his-set-box { padding-bottom: 10px; padding-top: 10px;}
.booking-his-set { padding: 0px 20px 0px 20px;}
.bok-mid-set { border-left: 2px solid #FFF; border-right: 2px solid #FFF; }
.total-set p { color: #1b5880; font-weight: bold;}
.booking-cont-set-main h4 { font-size: 18px; line-height: 20px;}

.book-sum-left span { float: right;}

.success-set { width: 80px; height: 80px; border-radius: 50%; overflow: hidden; margin:0 auto 20px;}


@media all and (max-width: 991px) and (min-width: 320px) {

.bok-his-set-main { padding: 0px 0px 0px 0px !important;}

}
	
</style>
<?php


?>




<header class="nps-header">
   <div class="auto-container">
       <div class="row clearfix">
           <div class="col-md-12 col-sm-12 no-left-right-padding">
           
                <div class="col-lg-3 col-md-3 col-sm-12 pl-0 pr-0">
                     <div class="logo"><a href="payment"><img src="images/logo.png" alt=""></a></div>
                     <div class="mob-menu-icon"><img src="images/menu.png"></div><!--logo end-->
                     <div class="clear"></div> 
                </div>
                
                <div class="col-lg-9 col-md-9 col-sm-9 nps-menu menu pl-0 pr-0">
                     <nav id="primary_nav_wrap">
                          <ul>                                   
                              
                              <li class="nps-icon2"><a href="payment"> &nbsp;&nbsp; Online Payment</a></li>
                              <li class="nps-icon1"><a href="self-reading">Self Reading</a></li>
                              <li class="nps-icon3"><a href="register">Register</a></li>
                              
                              <div class="clear"></div>
                          </ul>
                          <div class="clear"></div>   
                     </nav>
                    <div class="clear"></div>
                </div>
           
           
           </div>
       </div>
   </div>
</header>



<div class="page-wrapper">

    
    <!--industry-->
    <section class="industry-section nps-pb-100">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                
                
                
         <div class="page-info">
        	<div class="auto-container"><br>
<br>

            	<div class="row clearfix">
						
						<?php
						if($insertmsg==1)
						{
						?>
                      
                      <div class="success-set"><img src="images/success.gif" alt=""></div>
                
                
                     <h2 class="text-center" style="line-height: 34px;"><strong>THANK YOU</strong><br>Your reading has been submitted!</h2>
					 
					 <?php
						} else {
					 ?>
					 <h2 class="text-center" style="line-height: 34px;">Error!Your reading is not submitted!</h2>
					 <?php
						}
					 ?>
 
 
 
              </div>
            </div>
        </div>
                
                
            </div>
        </div>
    </section>
    
    <!--services-style-three-->
    <section class="services-style-three">
    	<div class="auto-container">
        	<div class="row clearfix">
            
            
            







            
            	
                
                
                
                
                
                
            </div>
        </div>
    </section>
    
        




    
</div>



<footer class="nps-footer">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-md-12 col-sm-12 no-left-right-padding">
            
              <div class="col-md-5 col-sm-12 nps-foot-left no-left-right-padding">
                <p><strong>Member of HappyWay Group</strong><br>Petrosafe © 2020 All Rights Reserved.</p>
              </div>
                 
                 <div class="col-md-2 col-sm-12 nps-foot-mid no-left-right-padding"><img src="../images/happy-logo.png"></div>
              <div class="col-md-5 col-sm-12 nps-foot-right no-left-right-padding">
                      <p><strong>Phone : +971 4 267 7294<br>Email  : utility@petrosafe.ae</strong></p> 
              </div>
            
            </div>
        </div>
    </div>
</footer>



<script src="js/jquery.js"></script> 



<!--End Google Map APi-->
<script>
$( document ).ready(function() {
	
	
	$('.mob-menu-icon') .click(function(){
  $('.menu').toggle(1000);
  });
  
  
  $('.mob-menu-hide') .click(function(){
  $('.menu').hide(1000);
  });
  
  });
	</script>



</body>
</html>