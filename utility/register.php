<!doctype html>
<base href="https://petrosafeme.com/">
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="utility/image/png" href="images/em-favicon.png"/>
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="utility/css/bootstrap.css" >
    <link rel="stylesheet" type="text/css" href="utility/css/style.css"/>
    
    <link rel="stylesheet" type="text/css" href="utility/css/font-awesome.min.css"/>

    <title>Petrosafe</title>
</head>
<body>



<header class="nps-header">
   <div class="auto-container">
       <div class="row clearfix">
           <div class="col-md-12 col-sm-12 no-left-right-padding">
           
                <div class="col-lg-3 col-md-3 col-sm-12 pl-0 pr-0">
                     <div class="logo"><a href="utility/payment"><img src="utility/images/logo.png" alt=""></a></div>
                     <div class="mob-menu-icon"><img src="utility/images/menu.png"></div><!--logo end-->
                     <div class="clear"></div> 
                </div>
                
                <div class="col-lg-9 col-md-9 col-sm-9 nps-menu menu pl-0 pr-0">
                     <nav id="primary_nav_wrap">
                          <ul>                                   
                              
                              <li class="nps-icon2"><a href="utility/payment"> &nbsp;&nbsp; Online Payment</a></li>
                              <li class="nps-icon1"><a href="utility/self-reading">Self Reading</a></li>
                              <li class="nps-icon3 active"><a href="utility/register.php">Register</a></li>
                              
                              <div class="clear"></div>
                          </ul>
                          <div class="clear"></div>   
                     </nav>
                    <div class="clear"></div>
                </div>
           
           
           </div>
       </div>
   </div>
</header>




<section class="industry-section mr-set-rate nps-pb-100">
    <div class="auto-container">
      <div class="row clearfix">
      
           <h2 class="text-center">Register</h2>
           
           <div class="col-md-12 col-sm-12  no-left-right-padding">
                
                <p>Content Soon</p>
                 
           </div>
           
      </div>
    </div>
  </section>





<footer class="nps-footer">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-md-12 col-sm-12 no-left-right-padding">
            
              <div class="col-md-5 col-sm-12 nps-foot-left no-left-right-padding">
                <p><strong>Member of HappyWay Group</strong><br>Petrosafe © 2020 All Rights Reserved.</p>
              </div>
                 
                 <div class="col-md-2 col-sm-12 nps-foot-mid no-left-right-padding"><img src="images/happy-logo.png"></div>
              <div class="col-md-5 col-sm-12 nps-foot-right no-left-right-padding">
                      <p><strong>Phone : +971 4 267 7294<br>Email  : utility@petrosafe.ae</strong></p> 
              </div>
            
            </div>
        </div>
    </div>
</footer>

<script src="utility/js/jquery.js"></script> 
<script>
$(document).ready(function() {
  
  $('.mob-menu-icon') .click(function(){
  $('.menu').toggle(1000);
  });
  
  
  $('.mob-menu-hide') .click(function(){
  $('.menu').hide(1000);
  });

});
</script>

  </body>
</html>