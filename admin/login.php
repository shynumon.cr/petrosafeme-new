<?php 
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
include('../utility/connection.php'); 
session_start();
$adminid=$_SESSION['adminid'];
if(is_numeric($adminid)){header("Location: dashboard.php");}
if(isset($_POST['login_submit']))
{
	$username=$_POST['txt_username'];
	$password=$_POST['txt_password'];//$_POST['txt_password'];
	
	//echo $username.",".$password;
	$loginQuery = "SELECT * FROM admin WHERE LoginName = '".$username."'  AND Password = '".$password."' AND usertype='old'";
	
	$resultLogin = mysqli_query($con,$loginQuery);
	$rowCount = mysqli_num_rows($resultLogin);
	$loginfail='';
	if($rowCount>0)
	{
		
		$rowResult = mysqli_fetch_row($resultLogin);
		$adminid=$rowResult[0];
		$_SESSION['adminid']=$adminid;
		$_SESSION['odooid']=$rowResult[3];
		// header("Location: dashboard.php");

        /*if(isset($_POST['new_web'])) {
            header("Location:laravel/public"); 
            exit();
        } else {
            header("Location: dashboard.php"); 
            exit();
        }*/
		
	}
    else
	{
		$loginfail=0;
	}
}
?>
<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Petrosafe Admin</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="https://www.petrosafeme.com/assets/img/favicon.png">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"> -->
    <link href="css/style.css" rel="stylesheet">
    
</head>

<body class="h-100">
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    



    <div class="login-form-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content">
                        <div class="card login-form mb-0">
                            <div class="card-body pt-5 text-center">
                                <a class="text-center" href="index.php">
								<img src="../utility/images/logo.png" alt="" style="width:150px;height:auto;margin:0px auto;">
								</a>
								<?php if($loginfail===0){ ?>
								<div class="alert alert-danger">Incorrect Username / Password !</div>
								<?php }?>
                                <form class="mt-5 mb-5 login-input" method="POST">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="txt_username" placeholder="Username" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="txt_password" placeholder="Password" required>
                                    </div>
                                    <!-- <div class="form-group row-cols-6">
                                    <label class='pr-3'>
                                        <input  type="radio" id="old_web" name="old_web" value="old_web" checked> Go to Old Site
                                    </label>
                                    <label>
                                        <input type="radio" id="new_web" name="new_web" value="new_web"> Go to New Site
                                    </label>
                                </div> -->
                                    <button type="submit" name="login_submit" class="btn login-form__btn submit w-100" value="1">Sign In</button>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  
    <script>
    $('#old_web').click(function() {
        $('#new_web').prop('checked', false); 
    });

    $('#new_web').click(function() {
        $('#old_web').prop('checked', false); 
    });
</script>
    

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
</body>
</html>





