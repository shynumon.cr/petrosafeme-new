<?php  
		session_start();
	    $adminid=$_SESSION['adminid'];
		$odoooid=$_SESSION['odooid'];
	    if(!is_numeric($adminid)){header("Location: index.php");}
	    include('../utility/connection.php');
		include('include/header.php'); 
		
		$buildingsQuery = "SELECT DISTINCT upd_cust_building_name FROM meter_reading_updates ORDER BY upd_cust_building_name";
	
	    $resultBuildings = mysqli_query($con,$buildingsQuery);
		
		
		$select_html='<div class="pull-right">Start Date&nbsp;<input value="" class="dt_tbl_ip fromdate"/>&nbsp;&nbsp;End Date&nbsp;<input value="" class="dt_tbl_ip todate"/>&nbsp;Building : <select id="build_search"><option value="">--All--</option>';
		
		while ($row = mysqli_fetch_assoc($resultBuildings)) 
		{
			$buildname=$row['upd_cust_building_name'];
			$select_html.='<option value="'.$buildname.'">'.$buildname.'</option>';
		}
		
		$select_html.='</select></div>';
		
		$i=1;
	   
?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Reading Updates</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Reading Updates</h4>
								<div class="text-right">
									<button type="button" id="exp_read_btn" class="btn mb-1 btn-info">Export<span class="btn-icon-right"><i class="fa fa-file-excel-o"></i></span>
                                    </button>
								</div>
                                <div class="table-responsive">
                                    <table class="table responsive table-striped table-bordered" id="read_table">
                                        <thead>
                                            <tr>
                                                <th>Sl.No</th>
												<th>Customer ID</th>
                                                <th>Customer Name</th>
												<th>Building</th>
                                                <th>Date & Time</th>
                                                <th>Meter ID</th>
                                                <th>Last Reading date</th>
                                                <th>Image</th>
                                                <th>Reading</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        
                                    </table>
                                </div>
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
		<input type="hidden" id="base_url" value="https://petrosafeme.com/admin/export_readings.php"/>
		<a href="#"	id="exportlink" class="d-none" ></a>
        <!--**********************************
            Content body end
        ***********************************-->
        
<?php  include('include/footer.php'); ?>        
        
    

    