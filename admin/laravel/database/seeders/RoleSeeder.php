<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Admin;
use App\Models\Permission;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoleSeeder extends Seeder
{
    public function run(): void
    {
        $category = new Category();
        $category->permission_category = 'Common';
        $category->save();

        $permission = new Permission();
        $permission->name = 'All Permissions';
        $permission->slug = 'all-permissions';
        $permission->category = $category->id;
        $permission->save();

        $role = new Role();
        $role->name = 'Super Admin';
        $role->slug = 'superadmin';
        $role->save();
        $role->permissions()->attach($permission);
        //$permission->roles()->attach($role);

        $superadmin = Role::where('slug', 'superadmin')->first();
        $all_permissions = Permission::where('slug', 'all-permissions')->first();

        $superadmin = new Admin();
        $superadmin->LoginName = 'Superadmin';
        $superadmin->email = 'superadmin@gmail.com';
        $superadmin->password = bcrypt('superadmin123');
        $superadmin->save();
        $superadmin->roles()->attach($superadmin);
        $superadmin->permissions()->attach($all_permissions);

        //Category::create(['permission_category' => 'User']);
        //Permission::create(['name' => 'view users']);
    }
}