<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bookingTypes = [
            ['booking_type' => 'CON', 'booking_type_name' => 'Connection'],
            ['booking_type' => 'DIS', 'booking_type_name' => 'Disconnection'],
            ['booking_type' => 'SHIF', 'booking_type_name' => 'Shifting'],
            ['booking_type' => 'RECON', 'booking_type_name' => 'Reconnection'],
        ];

        // Insert data into the database
        DB::table('booking_type')->insert($bookingTypes);
    }
}
