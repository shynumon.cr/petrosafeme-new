$(document).ready(function () {
    $("#new_building_div").hide();
    $("#new_flat__div").hide();
    $("#shifting_to_div").hide();
    $("#gas_available_div").hide();
    $("#availabilty_div").hide();
    $("#dewa_conect_div").hide();
    $("#pay_mode_div").hide();
    $("#eid_front_div").hide();
    $("#eid_back_div").hide();
    $("#tenacy_div").hide();
    $("#meter_div").hide();
    $("#gas_cooker_div").hide();
    $("#gas_contract_div").hide();
    $("input[name='booking_type']").change(function (e) {
        var booking_type = $(this).val();
        if (booking_type == 'CON') {
            $("#availabilty_div").hide();
            $("#gas_contract_div").hide();
            $("#new_building_div").hide();
            $("#new_flat__div").hide();
            $("#shifting_to_div").hide();

            $("#gas_available_div").show();
            $("#dewa_conect_div").show();
            $("#pay_mode_div").show();
            $("#eid_front_div").show();
            $("#eid_back_div").show();
            $("#tenacy_div").show();
            $("#meter_div").show();
            $("#gas_cooker_div").show();
        } else if (booking_type == 'DIS') {
            $("#gas_available_div").hide();
            $("#dewa_conect_div").hide();
            $("#pay_mode_div").hide();
            $("#tenacy_div").hide();
            $("#meter_div").hide();
            $("#gas_cooker_div").hide();
            $("#new_building_div").hide();
            $("#new_flat__div").hide();
            $("#shifting_to_div").hide();

            $("#eid_front_div").show();
            $("#eid_back_div").show();
            $("#gas_contract_div").show();
            $("#availabilty_div").show();

        } else if (booking_type == 'SHIF') {
            $("#meter_div").hide();
            $("#gas_cooker_div").hide();
            $("#gas_contract_div").hide();
            $("#availabilty_div").hide();


            $("#new_building_div").show();
            $("#new_flat__div").show();
            $("#shifting_to_div").show();
            $("#eid_front_div").show();
            $("#eid_back_div").show();
            $("#tenacy_div").show();
            $("#pay_mode_div").show();
            $("#gas_available_div").show();
            $("#dewa_conect_div").show();

        } else if (booking_type == 'RECON') {
            $("#pay_mode_div").hide();
            $("#tenacy_div").hide();
            $("#meter_div").hide();
            $("#gas_cooker_div").hide();
            $("#gas_contract_div").hide();
            $("#availabilty_div").hide();
            $("#new_building_div").hide();
            $("#new_flat__div").hide();
            $("#shifting_to_div").hide();
            $("#eid_front_div").hide();
            $("#eid_back_div").hide();
            $("#gas_available_div").hide();
            $("#dewa_conect_div").hide();
        }
    });
});

// *********************************************************

// submit booking
var booking_form = $("#booking_form").validate({
    ignore: [],
    rules: {
        customer_name: {
            required: true
        },
        mob_no: {
            required: true
        },

        address: {
            required: true
        },
        flat_no: {
            required: true
        },
        building_name: {
            required: true
        },
        whatsapp_no: {
            required: true
        },
        service_date: {
            required: true
        },
        booking_type: {
            required: true
        },
        team: {
            required: true
        },
        area: {
            required: true
        },
        slot_id: {
            required: true
        },
        comments: {
            required: true
        },
        // new_building_name: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'SHIF';
        //     }
        // },
        // new_flat_no: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'SHIF';
        //     }
        // },
        // shifting_to: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'SHIF';
        //     }
        // },
        // gas_available: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'CON' || $("#booking_type").val() === 'SHIF';
        //     }
        // },
        // availability: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'DIS';
        //     }
        // },
        // active_dewa_connection: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'CON' || $("#booking_type").val() === 'SHIF';
        //     }
        // },
        // payment_mode: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'CON' || $("#booking_type").val() === 'SHIF';
        //     }
        // },
        // eid_front: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'CON' || $("#booking_type").val() === 'DIS'
        //             || $("#booking_type").val() === 'SHIF';
        //     }
        // },
        // eid_back: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'CON' || $("#booking_type").val() === 'DIS'
        //             || $("#booking_type").val() === 'SHIF';
        //     }
        // },
        // tenancy: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'CON' || $("#booking_type").val() === 'SHIF';
        //     }
        // },
        // meter_picture: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'CON';
        //     }
        // },
        // gas_cooker_photo: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'CON';
        //     }
        // },
        // gas_contract_type: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'DIS';
        //     }
        // },
        // gas_contract_image: {
        //     required: function (element) {
        //         return $("#booking_type").val() === 'DIS';
        //     }
        // },

    },
    messages: {

    },
    errorPlacement: function (err, ele) {
        if (err.element == "availability") {
            err.insertAfter(ele.parent());
        } else {
            err.insertAfter(ele);
        }
    },
});
$('#booking_form #sbmt_btn').click(function () {
    // booking_form.resetForm();
    if ($("#booking_form").valid()) {
        console.log($("#booking_form").serialize());
        $(".submit_button").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop(
            'disabled', true);
        fm = new FormData(document.getElementById("booking_form"));
        $.ajax({
            url: 'add-edit-booking',
            type: 'post',
            data: fm,
            cache: false,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                if (data.status == 'success') {
                    $("#booking_form")[0].reset();
                    $(".add-member-popup-wrapper").hide(500);
                    swal("Success!", data.message, "success");
                    $(".submit_button").html('Save').prop('disabled', false);
                } else {
                    $(".add-member-popup-wrapper").hide(500);
                    $(".submit_button").html('Save').prop('disabled', false);
                    swal("Error!", data.message, "error");
                }
            },
            error: function (xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    } else {
        console.log('not valid form');
    }
});



$("input[name=booking_type]").change(function () {

    var bookingType = $(this).val();

    // Fields for 'CON' booking type
    if (bookingType === 'CON') {

        $("#gas_available, #active_dewa_connection, #payment_mode, #eid_front, #eid_back, #tenancy, #meter_picture, #gas_cooker_photo").each(function () {
            $(this).rules('add', { required: true });
        });
    } else {
        $("#gas_available, #active_dewa_connection, #payment_mode, #eid_front, #eid_back, #tenancy, #meter_picture, #gas_cooker_photo").each(function () {
            $(this).rules('remove', 'required');
        });
    }

    // Fields for 'SHIF' booking type
    if (bookingType === 'SHIF') {
        $("#new_building_name, #new_flat_no, #shifting_to,#gas_available, #active_dewa_connection, #payment_mode, #eid_front, #eid_back, #tenancy").each(function () {
            $(this).rules('add', { required: true });
        });
    } else {
        $("#new_building_name, #new_flat_no, #shifting_to,#gas_available, #active_dewa_connection, #payment_mode, #eid_front, #eid_back, #tenancy").each(function () {
            $(this).rules('remove', 'required');
        });
    }

    if (bookingType === 'DIS') {
        $("#availability, #eid_front, #eid_back, #gas_contract_type, #gas_contract_image").each(function () {
            $(this).rules('add', { required: true });
        });
    } else {
        $("#availability, #eid_front, #eid_back, #gas_contract_type, #gas_contract_image").each(function () {
            $(this).rules('remove', 'required');
        });
    }
    $("#new_building_name").val('');
    $("#new_flat_no").val('');
    $("#shifting_to").val('');
    $("#gas_available").val('');
    $("#availability").val('');
    $("#active_dewa_connection").val('');
    $("#payment_mode").val('');
    $("#eid_front").val('');
    $("#eid_back").val('');
    $("#tenancy").val('');
    $("#meter_picture").val('');
    $("#gas_cooker_photo").val('');
    $("#gas_contract_type").val('');
    $("#gas_contract_image").val('');
    // booking_form.resetForm();
    $(".input-field").removeClass("error");
});


$.validator.addMethod('fileSelected', function(value, element) {
    return $(element).get(0).files.length !== 0;
}, 'Please select a file.');

$("input[name=booking_type]").change(function() {
    var bookingType = $(this).val();

    if (bookingType === 'CON') {
        $("#eid_front, #eid_back, #tenancy").each(function() {
            $(this).rules('add', {
                fileSelected: true
            });
        });
    } else {
        $("#eid_front, #eid_back, #tenancy").each(function() {
            $(this).rules('remove', 'fileSelected');
        });
    }

});
