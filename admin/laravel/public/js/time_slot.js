
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
// *****************************************
// GET BOOKING COUNT
$(document).ready(function () {

    $('#slot_id').change(function () {
        var slotId = $(this).val();
        if (slotId != '') {
            $.ajax({
                url: 'get-booking-count',
                type: 'GET',
                data: { slot_id: slotId },
                success: function (response) {
                    $('#booking_count').val(response.booking_count);
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }
    });
});

// *****************************************************************

// time_slot Form submission
$(document).ready(function () {
    $('#time_slot_form').validate({
        rules: {
            slot_date: {
                required: true
            },
            team_id: {
                required: true
            },
            slot_id: {
                required: true
            },
            booking_count: {
                required: true,
                min: 1,
                digits: true
            }
        },
        messages: {
            booking_count: {
                min: "Booking count must be greater than or equal to 1",
                digits: "Please enter a valid number"
            },
            slot_date: 'Select a Slot Date',
            team_id: 'Select a Team',
            slot_id: 'Select a Time Slot',
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            $(".submit_button").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop('disabled', true);
            var fm = new FormData(form);
            $.ajax({
                url: 'team-slot-add',
                type: 'post',
                data: fm,
                cache: false,
                processData: false,
                contentType: false,
                success: function (data) {
                    console.log(data);
                    if (data.status == 'success') {
                        $("#time_slot_form")[0].reset();
                        $(".add-member-popup-wrapper").hide(500);
                        swal("Success!", data.message, "success");
                        $(".submit_button").html('Save').prop('disabled', false);
                        // location.reload();
                        // window.calendar.refetchEvents();
                        var newEvent = {
                            team_slot_id: data.event.team_slot_id,
                            title: data.event.team_name,
                            start: data.event.slot_date,
                            team_id: data.event.team_id,
                            slot_id: data.event.slot_id,
                            from_slot: data.event.from_slot,
                            to_slot: data.event.to_slot,
                            booking_count: data.event.booking_count,
                            extendedProps: {
                                team_name: data.event.team_name,
                                team_id: data.event.team_id,
                                slot_id: data.event.slot_id,
                                booking_count: data.event.booking_count
                            }
                        };
                        window.calendar.addEvent(newEvent);
                    } else {
                        $(".add-member-popup-wrapper").hide(500);
                        $(".submit_button").html('Save').prop('disabled', false);
                        swal("Error!", data.message, "error");
                    }
                },
                error: function (xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }
    });
});


// ***********************************************************
