<script type="text/javascript" src="<?php echo e(URL::asset('js/jquery-3.6.0.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/popper.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/datatables.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/dataTables.fixedHeader.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/bootstrap-datepicker.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/sweetalert.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/select2.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/raphael-min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/morris.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/common.js?v='.jsVersion())); ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('body').tooltip({ // bootsrap tooltip enable in datatable (dynamic)
            selector: '[data-toggle="tooltip"]'
        });
        $(".menu-icon").click(function() {
            $(".main-nav").toggle(500);
        });
        $(".top-dropdown-set").click(function() {
            $(".top-dropdown").toggle(500);
        });
        $(".close-btn").click(function() {
            $(".login-wrapper, .add-member-popup-wrapper, .common-popup-wrapper").hide(500);
        });
        $(".tooltip-ation-main").click(function() {
            $('.tooltip-ation-main').removeClass('active');
            $(this).toggleClass('active');
        });
    });
    $(document).on('click', '.tooltip-ation-main', function() {
        if ($(this).find('.tooltip-ation').is(':visible')) {
            $('.tooltip-ation').hide();
            return false;
        }
        $('.tooltip-ation').hide();
        $(this).find('.tooltip-ation').show();
    });
</script>
<?php echo $__env->yieldPushContent('scripts'); ?>
<?php /**PATH C:\xampp\htdocs\petrosafeme\admin\laravel\resources\views/includes/main_footer_assets.blade.php ENDPATH**/ ?>