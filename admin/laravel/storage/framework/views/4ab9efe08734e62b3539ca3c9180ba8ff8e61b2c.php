<?php $__env->startSection('content'); ?>
<div class="row login-section m-0">
        <div class="col-lg-8 col-md-6 login-left p-0">
            <h1 class="MyriadPro-Bold text-capital"><span class="MyriadPro-Regular">Welcome to</span><br>PetroSafe</h1>
        </div>
        <div class="col-lg-4 col-md-6 login-right d-flex mx-auto my-auto">
            <form method="POST" action="<?php echo e(route('register')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="login-main mx-auto">
                    <div class="col-sm-12 login-logo">
                        <img src="<?php echo e(URL::asset('images/logo.png')); ?>" class=" mx-auto">
                    </div>
                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-user"></i>
                                <input id="LoginName" type="text" class="input-field  <?php $__errorArgs = ['LoginName'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="LoginName" value="<?php echo e(old('LoginName')); ?>" placeholder="Name" required autocomplete="LoginName" autofocus>

                                <?php $__errorArgs = ['LoginName'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            
                            </div>
                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-envelope"></i>
                                <input id="email" type="email" class="input-field  <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="email" value="<?php echo e(old('email')); ?>" placeholder="Email" required autocomplete="email">

                                <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            
                            </div>
                            <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-user"></i>
                                <input id="username" type="text" class="input-field  <?php $__errorArgs = ['username'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="username" value="<?php echo e(old('username')); ?>" placeholder="Username" required autocomplete="username" autofocus>

                                <?php $__errorArgs = ['username'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            
                            </div>
                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-unlock-alt"></i>
                                <input id="password" type="password" class="input-field  <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="password" required placeholder="Password"autocomplete="new-password">

                                <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            
                        </div>
                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-unlock-alt"></i>
                                <input id="password-confirm" type="password" class="input-field " name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">
                            </div>
                       
                            <div class="col-sm-12 cm-field-btn p-0 m-0">
                                <button type="submit" class="btn btn-primary">
                                    <?php echo e(__('Register')); ?>

                                </button>
                            </div>
                        
                    </form>
            <!-- <footer>
                <div class="az-section">
                    <a href="http://azinovatechnologies.com/" target="_blank">
                        <div class="azinova-logo"></div>
                    </a>
                    <p class="no-padding">Powered by :</p>
                    <div class="clear"></div>
                </div>
            </footer> -->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\petrosafme\admin\laravel\resources\views/auth/register.blade.php ENDPATH**/ ?>