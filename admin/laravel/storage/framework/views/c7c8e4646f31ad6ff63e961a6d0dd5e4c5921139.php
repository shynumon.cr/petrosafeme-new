
<?php $__env->startSection('content'); ?>
    
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <div class="add-member-popup-wrapper">
	<div class="add-member-popup-section d-flex">
		<div class="add-member-popup-main col-sm-5 mx-auto">
			<div class="close-btn"><img src="<?php echo e(URL::asset('images/white-close.png')); ?>" alt=""></div>
			<h5 class="MyriadPro-Bold modal_title">Add User</h5>
			<div class="col-12 step-cont" id="cont1">
				<form id="user_form"  method="post">
					<div class="row m-0">
						<div class="col-sm-6 cm-field-main cm-field">
							<p>Name<span class="text-danger">*</span></p>
							<input class="input-field" placeholder="Name" name="LoginName" type="text" id="LoginName">
							<input type="hidden" name="id">
						</div>
                        <div class="col-sm-6 cm-field-main cm-field">
                            <p>User Type<span class="text-danger">*</span></p>
                            <select name="usertype" id="usertype" class="input-field" disabled>
                                <option value="new" selected>New</option>
                                <option value="old">Old</option>
                            </select>
                        </div>
                        <div class="col-sm-6 cm-field-main cm-field">
                            <p>Email<span class="text-danger">*</span></p>
                            <input class="input-field" placeholder="Email" name="email" type="email" id="email">
                            <input type="hidden" name="id">
                        </div>
                        <div class="col-sm-6 cm-field-main cm-field">
                            <p>Username<span class="text-danger">*</span></p>
                            <input class="input-field" placeholder="Username" name="username" type="text" id="username">
                        </div>
                        <!-- <div id="change_pass_fields" style="display: none;">
                            <p>Change password?

                            </p>
                        </div> -->
                        <!-- <div id="pass_fields"> -->
    					<div class="col-sm-6 cm-field-main cm-field passwordsec" id="passwordsec">
    							<p>Password<span class="text-danger">*</span></p>
    							<input class="input-field" placeholder="Password" name="password" id="password" type="password">
    						</div>
                        <div class="col-sm-6 cm-field-main cm-field passwordsec" >
                                <p>Confirm Password<span class="text-danger">*</span></p>
                                <input class="input-field" placeholder="Conform Password" name="conform_password" id="conform_password" type="password">
                        </div>
						<div class="col-sm-6 cm-field-main cm-field">
							<p>Role<span class="text-danger">*</span></p>
							<select name="role" id="role" class="input-field">
								<option value="">-- Select Role --</option>
                                <?php if(!empty($roles)): ?>
                                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $each_role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($each_role->id); ?>"><?php echo e(ucfirst($each_role->name)); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
								
							</select>
						</div>
                        <div class="col-sm-6 cm-field-main cm-field">
                            <p>Status<span class="text-danger">*</span></p>
                            <select name="status" id="status" class="input-field">
                                <option value="">-- Select Status --</option>
                                <option value="active">Active</option>
                                <option value="disabled">Disabled</option>
                            </select>
                        </div>
						
						<div class="col-sm-12 cm-field-btn">
							<button type="submit" class="submit_buttom field-btn CM font-weight-normal w-auto">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>






    <div class="common-popup-wrapper" id="delete_popup" style="display:none;">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main mx-auto col-lg-4 col-md-6 col-sm-6">
                <div class="close-btn"><img src="<?php echo e(URL::asset('images/white-close.png')); ?>" alt=""></div>
                <div class="common-popup-icon m-auto"><img src="<?php echo e(URL::asset('images/alert.gif')); ?>" alt=""></div>
                <h5 class="MyriadPro-Bold text-center">Are you sure to delete</h5>
                <div class="common-popup-text">
                    <div class="col-12 cm-field-main cm-field p-0 pr-0 text-center mt-4">
                        <input type="button" class="field-btn CM font-weight-normal w-auto Danger" value="Delete">
                        <input type="button" class="field-btn font-weight-normal w-auto" value="Cancel">
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Pop Up -->

    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">User List</h4>
                </li>
                
            </ul>
        </div>
        <!--page-title-main end-->

        <div class="col-12 cm-content-main">
            <div class="col-12 table-main">
                <div class="col-12 table-filter">
                    <ul>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Search Keyword</p>
                                <input class="input-field" placeholder="Search Here" id="search" type="text">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Start Date</p>
                                <input class="form-control input-field calendar" placeholder="Start Date" id="start_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>End Date</p>
                                <input class="form-control input-field calendar" placeholder="End Date" id="end_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="search()"><i class="fa fa-search"></i>Search</span>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Sort by</p>
                                <select name="" id="sub_status" class="time-slot" onchange="sortData()";>
                                    <option value="" data-url="">Select from list</option>
                                    <option value="name" data-url="">Name</option>
                                </select>

                            </div>
                        </li>

                        <li>
                            <div class="cm-field-main m-0">
                                <p>Show By</p>
                                <select name="show_by" id="show_by">
                                    <?php $__currentLoopData = [10, 25, 50, 100]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $it): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option <?php echo e($it == 50 ? 'selected' : ''); ?> value="<?php echo e($it); ?>">
                                            <?php echo e($it); ?> entries</option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </li>
                        
                        <li class="float-right text-right width-auto">
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="add_product();"><i class="fa fa-plus"></i>New</span>
                            </div>
                        </li>
                        
                    </ul>
                </div>

                <table id="user_table" class="table table-sm table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>
                                Sl. No.
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Username
                            </th>
                            <th>
                                Created Time
                            </th>
                            <th>
                                Status
                            </th>
                            <th class="action">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!--cm-content-main end-->
        </div>
        <!--cm-content-section end-->
    <?php $__env->stopSection(); ?>
    <?php $__env->startPush('scripts'); ?>
        <script type="text/javascript" src="<?php echo e(URL::asset('js/jquery.validate.js')); ?>"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var studentTable = '';
            $(document).ready(function() {
                $("#start_date,#end_date").datepicker({
                    autoclose: true
                });
                studentTable = $('#user_table').DataTable({
                    'bFilter': false,
                    'bLengthChange': false,
                    'pageLength': 50,
                    'processing': true,
                    'serverSide': true,
                    'bSort': false,
                    'serverMethod': 'post',
                    //  "bDestroy": true,
                    'ajax': {
                        'url': '<?php echo e(Url('list-users')); ?>',
                        'data': function(data) {
                            data.from_date = dmy($('#start_date').val());
                            data.to_date = dmy($('#end_date').val());
                            data.keywordsearch = $('#search').val();
                            data.sub_status = $('#sub_status').val();
                        },
                        "complete": function(json, type) {
                            var _resp = $.parseJSON(json.responseText);
                            console.log(_resp);
                        }
                    },
                    'columns': [{
                            data: 'slno'
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'email'
                        },
                        {
                            data: 'username'
                        },
                        {
                            data: 'addedtime'
                        },
                        {
                            data: 'status'
                        },
                        {
                            data: 'action'
                        },
                    ]
                    
                });

                $("#user_form").validate({
                    ignore: [],
                    rules: {
                        LoginName: {
                            required: true
                        },
                        email: {
                            required: true
                        },
                        /*password: {
                            required: true
                        },
                        conform_password: {
                            required: true
                        },*/

                        role: {
                            required: true
                        },

                        status: {
                            required: true
                        },
                        // role: {
                        //     required: true
                        // },
                    },
                    errorPlacement: function(err, ele) {
                        err.insertAfter(ele);
                    },
                    submitHandler: function(form, element) {
                        $(".submit_buttom").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop(
                            'disabled', true);
                        fm = new FormData(form);
                        $.ajax({
                            url: '<?php echo e(Url('add-edit-user')); ?>',
                            type: 'post',
                            data: fm,
                            cache: false,
                            processData: false,
                            contentType: false,
                            success: function(data) {
                                console.log(data);
                                if (data.status == 'success') {
                                    $("#user_form")[0].reset();
                                    $(".add-member-popup-wrapper").hide(500);
                                    studentTable.draw();
                                    swal("Success!", data.message, "success");
                                    $(".submit_buttom").html('Save').prop('disabled', false);
                                } else {
                                    $(".add-member-popup-wrapper").hide(500);
                                    $(".submit_buttom").html('Save').prop('disabled', false);
                                    swal("Error!", data.message, "error");
                                }
                            },
                            error: function(xhr, status, error) {
                                console.error(xhr.responseText);
                            }
                        });
                    }
                })

                $('#show_by').change(function() {
                    studentTable.page.len($(this).val()).draw();
                })
            });

            function add_product() {
                $('#passwordsec').show();
                $("#password").rules('add', {
                    required: true
                })
                $(".submit_buttom").html('Save');
                $('[name=id]').val('');
                $('.modal_title').html('Add User');
                $("#user_form")[0].reset();
                $(".add-member-popup-wrapper").show(500);
            }


            function delete_user(id) {
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this User!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: '<?php echo e(Url('delete-user')); ?>',
                                type: 'post',
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    if (data.status == 'success') {
                                        studentTable.draw();
                                        swal("Success!", data.message, "success");
                                    } else {
                                        swal("Error!", data.message, "error");
                                    }
                                }

                            })
                        }
                    });
            }

            function search() {
                studentTable.draw();
            }

            function sortData() {
                studentTable.draw();
            }

            function edit_user(id) {
                $.ajax({
                    url: '<?php echo e(Url('get-users')); ?>',
                    type: 'post',
                    data: {
                        id: id
                    },
                    success: function(data) {
                        //console.log(data);
                        if (data.status == 'success') {
                            $('#pass_fields').hide();
                            //$('#change_pass_fields').show();
                            put_fields(data.data);
                        } else {
                            swal("Error!", data.message, "error");
                        }
                    }
                })
            }

            function put_fields(a) {
                $("[name=LoginName]").val(a.LoginName);
                $("[name=username]").val(a.username);
                $("[name=email]").val(a.email);
                console.log(a);
                if(a.roles){
                    if(a.roles[0]){
                        console.log(a.roles[0]);
                        $("[name=role]").val(a.roles[0].id);
                    }
                }
                
                $("#status").val(a.status);
                $("[name=Password]").val(a.password);
                 $('.passwordsec').hide();
                $("[name=id]").val(a.id);
                $('.modal_title').html('Edit User');
                $(".submit_buttom").html('Update');
                $(".add-member-popup-wrapper").show(500);
            }

            function dmy(a) {
                if (a == '')
                    return '';
                a = a.split('/');
                b = [];
                b.push(a[2]);
                b.push(a[0]);
                b.push(a[1]);
                b = b.join('-');
                return b;
            }
        </script>
        <style>
            .error {
                color: red;
                font-size: 13px;
                display: block;
                text-align: left;
            }
        </style>
    <?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\petrosafme\admin\laravel\resources\views/users/users-list.blade.php ENDPATH**/ ?>