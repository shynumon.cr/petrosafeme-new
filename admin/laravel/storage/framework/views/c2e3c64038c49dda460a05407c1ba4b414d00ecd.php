<!doctype html>
<html lang="en">
<head>
    <title>Petrosafe - <?php echo $__env->yieldContent('title','Admin'); ?></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="theme-color" content="#d5e5f4">
    <?php echo $__env->make('includes.main_header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</head>
<body>
    <div class="wrapper-main">
        <?php echo $__env->make('includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <style>
        body .select2-container.select2-container--open {
            z-index: 99999999 !important;
        }
        </style>
        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->make('includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->make('includes.main_footer_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
</body>
</html>
<?php /**PATH C:\wamp64\www\petrosafme\admin\laravel\resources\views/layouts/main.blade.php ENDPATH**/ ?>