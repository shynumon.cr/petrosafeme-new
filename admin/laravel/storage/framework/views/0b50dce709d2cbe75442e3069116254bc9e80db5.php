<?php $__env->startSection('content'); ?>
    
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <div class="add-member-popup-wrapper">
   <div class="add-member-popup-section d-flex">
      <div class="add-member-popup-main col-sm-5 mx-auto">
         <div class="close-btn"><img src="<?php echo e(URL::asset('images/white-close.png')); ?>" alt=""></div>
         <h5 class="MyriadPro-Bold modal_title">Add Module</h5>
         <div class="col-12 step-cont" id="cont1">
            <form id="module_form" method="post">
               <div class="row m-0">
                  <div class="col-sm-12 cm-field-main cm-field">
                     <p>Module Name<span class="text-danger">*</span></p>
                     <input class="input-field" placeholder="Module Name" name="name" type="text" id="name">
                     <input type="hidden" name="id">
                  </div>
                  
                  
                  <div class="col-sm-12 cm-field-btn">
                     <button type="submit" class="submit_buttom field-btn CM font-weight-normal w-auto">Save</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>






    <div class="common-popup-wrapper" id="delete_popup" style="display:none;">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main mx-auto col-lg-4 col-md-6 col-sm-6">
                <div class="close-btn"><img src="<?php echo e(URL::asset('images/white-close.png')); ?>" alt=""></div>
                <div class="common-popup-icon m-auto"><img src="<?php echo e(URL::asset('images/alert.gif')); ?>" alt=""></div>
                <h5 class="MyriadPro-Bold text-center">Are you sure to delete</h5>
                <div class="common-popup-text">
                    <div class="col-12 cm-field-main cm-field p-0 pr-0 text-center mt-4">
                        <input type="button" class="field-btn CM font-weight-normal w-auto Danger" value="Delete">
                        <input type="button" class="field-btn font-weight-normal w-auto" value="Cancel">
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Pop Up -->

    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">Module List</h4>
                </li>
            </ul>
        </div>
        <!--page-title-main end-->

        <div class="col-12 cm-content-main">
            <div class="col-12 table-main">
                <div class="col-12 table-filter">
                    <ul>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Search Keyword</p>
                                <input class="input-field" placeholder="Search Here" id="search" type="text">
                            </div>
                        </li>
                        <!-- <li>
                            <div class="cm-field-main m-0">
                                <p>Start Date</p>
                                <input class="form-control input-field calendar" placeholder="Start Date" id="start_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>End Date</p>
                                <input class="form-control input-field calendar" placeholder="End Date" id="end_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li> -->
                        <li>
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="search()"><i class="fa fa-search"></i>Search</span>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Sort by</p>
                                <select name="" id="sub_status" class="time-slot" onchange="sortData()";>
                                    <option value="" data-url="">Select from list</option>
                                    <option value="permission_category" data-url="">Name</option>
                                </select>

                            </div>
                        </li>

                        <li>
                            <div class="cm-field-main m-0">
                                <p>Show By</p>
                                <select name="show_by" id="show_by">
                                    <?php $__currentLoopData = [10, 25, 50, 100]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $it): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option <?php echo e($it == 50 ? 'selected' : ''); ?> value="<?php echo e($it); ?>">
                                            <?php echo e($it); ?> entries</option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </li>
                       
                        <li class="float-right text-right width-auto">
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="add_product();"><i class="fa fa-plus"></i>New</span>
                            </div>
                        </li>
                        
                    </ul>
                </div>

                <table id="module_table" class="table table-sm table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>
                                Sl. No.
                            </th>
                            <th>
                                Module Name
                            </th>
                           <th class="action">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!--cm-content-main end-->
        </div>
        <!--cm-content-section end-->
    <?php $__env->stopSection(); ?>
    <?php $__env->startPush('scripts'); ?>
        <script type="text/javascript" src="<?php echo e(URL::asset('js/jquery.validate.js')); ?>"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var studentTable = '';
            $(document).ready(function() {
               /* $("#start_date,#end_date").datepicker({
                    autoclose: true
                });*/
                studentTable = $('#module_table').DataTable({
                    'bFilter': false,
                    'bLengthChange': false,
                    'pageLength': 50,
                    'processing': true,
                    'serverSide': true,
                    'bSort': false,
                    'serverMethod': 'post',
                    //  "bDestroy": true,
                    'ajax': {
                        'url': '<?php echo e(Url('list-modules')); ?>',
                        'data': function(data) {
                            /*data.from_date = dmy($('#start_date').val());
                            data.to_date = dmy($('#end_date').val());*/
                            data.keywordsearch = $('#search').val();
                            data.sub_status = $('#sub_status').val();
                        },
                        "complete": function(json, type) {
                            var _resp = $.parseJSON(json.responseText);
                            console.log(_resp);
                        }
                    },
                    'columns': [{
                            data: 'slno'
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'action'
                        },
                    ]
                    
                });











                $("#module_form").validate({
                    ignore: [],
                    rules: {
                        name: {
                            required: true
                        },
                        slug: {
                            required: true
                        }
                    },
                    errorPlacement: function(err, ele) {
                        err.insertAfter(ele);
                    },
                    submitHandler: function(form, element) {
                        $(".submit_buttom").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop(
                            'disabled', true);
                        fm = new FormData(form);
                        $.ajax({
                            url: '<?php echo e(Url('add-edit-module')); ?>',
                            type: 'post',
                            data: fm,
                            cache: false,
                            processData: false,
                            contentType: false,
                            success: function(data) {
                                console.log(data);
                                if (data.status == 'success') {
                                    $("#module_form")[0].reset();
                                    $(".add-member-popup-wrapper").hide(500);
                                    studentTable.draw();
                                    swal("Success!", data.message, "success");
                                    $(".submit_buttom").html('Save').prop('disabled', false);
                                } else {
                                    $(".add-member-popup-wrapper").hide(500);
                                    $(".submit_buttom").html('Save').prop('disabled', false);
                                    swal("Error!", data.message, "error");
                                }
                            },
                            error: function(xhr, status, error) {
                                console.error(xhr.responseText);
                            }
                        });
                    }
                })

                $('#show_by').change(function() {
                    studentTable.page.len($(this).val()).draw();
                })
            });

            function add_product() {
                
                $(".submit_buttom").html('Save');
                $('[name=id]').val('');
                $('.modal_title').html('Add Module');
                $("#module_form")[0].reset();
                $(".add-member-popup-wrapper").show(500);
            }


            function delete_module(id) {
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this module!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: '<?php echo e(Url('delete-module')); ?>',
                                type: 'post',
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    if (data.status == 'success') {
                                        studentTable.draw();
                                        swal("Success!", data.message, "success");
                                    } else {
                                        swal("Error!", data.message, "error");
                                    }
                                }

                            })
                        }
                    });
            }

            function search() {
                studentTable.draw();
            }

            function sortData() {
                studentTable.draw();
            }

            function edit_module(id) {
                $.ajax({
                    url: '<?php echo e(Url('get-module')); ?>',
                    type: 'post',
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                           // $('#pass_fields').hide();
                            //$('#change_pass_fields').show();
                            put_fields(data.data);
                        } else {
                            swal("Error!", data.message, "error");
                        }
                    }
                })
            }

            function put_fields(a) {
                $("[name=name]").val(a.permission_category);
                $("[name=id]").val(a.id);
                $('.modal_title').html('Edit Module');
                $(".submit_buttom").html('Update');
                $(".add-member-popup-wrapper").show(500);
            }

            function dmy(a) {
                if (a == '')
                    return '';
                a = a.split('/');
                b = [];
                b.push(a[2]);
                b.push(a[0]);
                b.push(a[1]);
                b = b.join('-');
                return b;
            }
        </script>
        <style>
            .error {
                color: red;
                font-size: 13px;
                display: block;
                text-align: left;
            }
        </style>
    <?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\petrosafme\admin\laravel\resources\views/category/categoryList.blade.php ENDPATH**/ ?>