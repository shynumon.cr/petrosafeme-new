
<?php $__env->startSection('title', 'Dashboard'); ?>
<?php $__env->startSection('content'); ?>
<div class="row cm-content-section m-0">
    <div class="col-12 page-title-main pl-0 pr-0">
        <ul>
            <li>
                <h4 class="MyriadPro-Bold">Dashboard</h4>
            </li>
            <li class="float-right">Last Login: <span
                    class="text-blue bold"><?php echo e(date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime))); ?></span>
            </li>
        </ul>
    </div>
</div>
<div class="card-body">
    <?php if(session('status')): ?>
        <div class="alert alert-success" role="alert">
            <?php echo e(session('status')); ?>

        </div>
    <?php endif; ?>
    <?php if(auth()->check() && auth()->user()->hasRole('admin')) : ?>
        <?php echo e(__('You are admin')); ?>

    <?php endif; ?>
    <?php if(auth()->check() && auth()->user()->hasRole('manager')) : ?>
        <?php echo e(__('You are user')); ?>

    <?php endif; ?>
<?php
/*echo "<pre>";
print_r(auth()->user()->permissions);
echo "<pre>";*/
    /*if(auth()->user()->hasPermissionTo('list-user')){
        echo "You can List user";
    }else{
        echo "can't List user";
    }*/
echo "<br/>";
    if(auth()->user()->can('list-user')){
        echo "You can List user";
    }else{
        echo "can't List user";
    }
    echo "<br/>";
    if(auth()->user()->can('all-permissions')){
        echo "You can all permission";
    }else{
        echo "can't all permission";
    }

    echo "<br/>";
    if(auth()->user()->can('create-user')){
        echo "You can Create user";
    }else{
        echo "can't Create user";
    }
echo "<br/>";
    if(auth()->user()->can('edit-user')){
        echo "You can Edit user";
    }else{
        echo "can't Edit user";
    }
    echo "<br/>";

    echo "<br/>";
    if(auth()->user()->can('delete-user')){
        echo "You can Edit user";
    }else{
        echo "Can't Edit user";
    }
    echo "<br/>";
?>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('styles'); ?>
        <link rel="stylesheet" href="<?php echo e(URL::asset('css/animate.min.css')); ?>" />
    <?php $__env->stopPush(); ?>
    
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\petrosafeme-new\admin\laravel\resources\views/home.blade.php ENDPATH**/ ?>