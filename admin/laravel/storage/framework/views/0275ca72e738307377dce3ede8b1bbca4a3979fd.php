<link rel="icon" type="image/png" href="<?php echo e(URL::asset('images/fav.png')); ?>"/>
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/bootstrap.css')); ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/style.css?v='.cssVersion())); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/font-awesome.min.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/owl.carousel.min.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/booking.css')); ?>"/>
<?php echo $__env->yieldPushContent('styles'); ?>
<?php /**PATH C:\wamp64\www\petrosafeme-new\admin\laravel\resources\views/includes/header_assets.blade.php ENDPATH**/ ?>