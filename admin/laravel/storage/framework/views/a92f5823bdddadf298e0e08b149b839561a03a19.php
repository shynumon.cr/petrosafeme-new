
<?php $__env->startSection('content'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <div class="add-member-popup-wrapper">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main col-sm-4 mx-auto">
                <div class="close-btn"><img src="<?php echo e(URL::asset('images/white-close.png')); ?>" alt=""></div>
                <h5 class="MyriadPro-Bold modal_title">Add Area</h5>
                <div class="col-12 step-cont" id="cont1">
                    <form id="area_form" class="row" method="post">
                        <div class="col-md-12">
                            <div class="col-12 cm-field-main cm-field pl-0 pr-0">
                                <p>Area Name</p>
                                <input class="input-field" placeholder="" name="area_name" type="text">
                            </div>

                            <div class="col-12 cm-field-main cm-field pl-0 pr-0">

                                <p>Team</p>
                                <select name="team_id" id="team_id">
                                    <option value="">---Select Team---</option>
                                    <?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($team->t_id); ?>"><?php echo e($team->team_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <input type="hidden" name="team_name" id="team_name">
                            </div>

                            <div class="col-12 cm-field-main cm-field pl-0 pr-0">

                                <p>Description</p>
                                <input class="input-field no-arrows" placeholder="" name="description" type="text">
                                <input type="hidden" name="id">
                            </div>

                            <div class="col-12 cm-field-btn p-0">
                                <button type="submit"
                                    class="submit_button field-btn CM font-weight-normal w-auto">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="common-popup-wrapper" id="delete_popup" style="display:none;">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main mx-auto col-lg-4 col-md-6 col-sm-6">
                <div class="close-btn"><img src="<?php echo e(URL::asset('images/white-close.png')); ?>" alt=""></div>
                <div class="common-popup-icon m-auto"><img src="<?php echo e(URL::asset('images/alert.gif')); ?>" alt=""></div>
                <h5 class="MyriadPro-Bold text-center">Are you sure to delete</h5>
                <div class="common-popup-text">
                    <div class="col-12 cm-field-main cm-field p-0 pr-0 text-center mt-4">
                        <input type="button" class="field-btn CM font-weight-normal w-auto Danger" value="Delete">
                        <input type="button" class="field-btn font-weight-normal w-auto" value="Cancel">
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Pop Up -->

    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">Areas</h4>
                </li>
                
            </ul>
        </div>
        <!--page-title-main end-->

        <div class="col-12 cm-content-main">
            <div class="col-12 table-main">
                <div class="col-12 table-filter">
                    <ul>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Search Keyword</p>
                                <input class="input-field" placeholder="Search Here" id="search" type="text">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Start Date</p>
                                <input class="form-control input-field calendar" placeholder="Start Date" id="start_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>End Date</p>
                                <input class="form-control input-field calendar" placeholder="End Date" id="end_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="search()"><i class="fa fa-search"
                                        aria-hidden="true"></i>Search</span>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Sort by</p>
                                <select name="" id="sub_status" class="time-slot" onchange="sortData()";>
                                    <option value="">Select from list</option>
                                    <option value="name.asc">Name A-Z</option>
                                    <option value="name.desc">Name Z-A</option>
                                    <option value="created_at.asc">Created ></option>
                                    <option value="created_at.desc">Created << /option>
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Show By</p>
                                <select name="show_by" id="show_by">
                                    <?php $__currentLoopData = [10, 25, 50, 100]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $it): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option <?php echo e($it == 25 ? 'selected' : ''); ?> value="<?php echo e($it); ?>">
                                            <?php echo e($it); ?>

                                            entries</option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </li>
                        <li class="float-right text-right width-auto">
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="add_area();"><i class="fa fa-plus"
                                        aria-hidden="true"></i> New</span>
                            </div>
                        </li>
                    </ul>
                </div>

                <table id="area_list_table" class="table table-sm table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>
                                Sl. No
                            </th>
                            <th>
                                Area Name
                            </th>
                            <th>
                                Description
                            </th>
                            <th>
                                Team Name
                            </th>
                            <th class="action">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!--cm-content-main end-->
        </div>
        <!--cm-content-section end-->
    <?php $__env->stopSection(); ?>
    <?php $__env->startPush('scripts'); ?>
        <script type="text/javascript" src="<?php echo e(URL::asset('js/jquery.validate.js')); ?>"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var studentTable = '';
            $(document).ready(function() {
                $("#start_date,#end_date").datepicker({
                    autoclose: true
                });
                studentTable = $('#area_list_table').DataTable({
                    'bFilter': false,
                    'bLengthChange': false,
                    'pageLength': 25,
                    'processing': true,
                    'serverSide': true,
                    'bSort': false,
                    'serverMethod': 'post',
                    //  "bDestroy": true,
                    'ajax': {
                        'url': '<?php echo e(url('list-area')); ?>',
                        'data': function(data) {
                            data.from_date = dmy($('#start_date').val());
                            data.to_date = dmy($('#end_date').val());
                            data.keywordsearch = $('#search').val();
                            data.sub_status = $('#sub_status').val();
                        },
                        "complete": function(json, type) { // data sent from controllerr
                            var _resp = $.parseJSON(json.responseText);
                        }
                    },
                    'columns': [{
                            data: 'slno'
                        },
                        {
                            data: 'area_name'
                        },
                        {
                            data: 'description'
                        },

                        {
                            data: 'team_name'

                        },
                        {
                            data: 'action'
                        },
                    ]
                });

                $("#area_form").validate({
                    ignore: [],
                    rules: {
                        area_name: {
                            required: true
                        },
                        team_id: {
                            required: true
                        },
                        description: {
                            required: true
                        },
                    },
                    errorPlacement: function(err, ele) {
                        if (ele.attr('name') == 'team_id') {
                            err.insertAfter(ele.parent());
                        }
                        err.insertAfter(ele);
                    },
                    messages: {
                        area_name: 'Area Name Required',
                        team_id: 'Team Name Required',
                        description: 'Description Required',
                    },
                    submitHandler: function(form, element) {
                        $(".submit_button").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop(
                            'disabled', true);
                        fm = new FormData(form);
                        $.ajax({
                            url: '<?php echo e(Url('add-edit-area')); ?>',
                            type: 'post',
                            data: fm,
                            cache: false,
                            processData: false,
                            contentType: false,
                            success: function(data) {
                                if (data.status == 'success') {
                                    $("#area_form")[0].reset();
                                    $(".add-member-popup-wrapper").hide(500);
                                    studentTable.draw();
                                    swal("Success!", data.message, "success");
                                    $(".submit_button").html('Save').prop('disabled', false);
                                } else {
                                    $(".add-member-popup-wrapper").hide(500);
                                    $(".submit_button").html('Save').prop('disabled', false);
                                    swal("Error!", data.message, "error");
                                }
                            },
                            error: function(data) {
                                $(".add-member-popup-wrapper").hide(500);
                                $(".submit_button").html('Save').prop('disabled', false);
                                swal("Error!", data.statusText, "error");
                            }
                        })
                    }
                })

                $('#show_by').change(function() {
                    studentTable.page.len($(this).val()).draw();
                })
            });

            function add_area() {
                $(".submit_button").html('Save');
                $('[name=id]').val('');
                $('.modal_title').html('Add Area');
                $("#area_form")[0].reset();
                $(".add-member-popup-wrapper").show(500);
            }

            function delete_area(id) {
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this Area!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: '<?php echo e(Url('delete-area')); ?>',
                                type: 'post',
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    if (data.status == 'success') {
                                        studentTable.draw();
                                        swal("Success!", data.message, "success");
                                    } else {
                                        swal("Error!", data.message, "error");
                                    }
                                },
                                error: function(data) {
                                    swal("Error!", data.statusText, "error");
                                }

                            })
                        }
                    });
            }


            function search() {
                studentTable.draw();
            }

            function sortData() {
                studentTable.draw();
            }

            function edit_area(id) {
                $.ajax({
                    url: '<?php echo e(Url('get-area')); ?>',
                    type: 'post',
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            put_fields(data.data);
                        } else {
                            swal("Error!", data.message, "error");
                        }
                    }

                })
            }

            function put_fields(a) {
                $("[name=area_name]").val(a.area_name);
                $("[name=description]").val(a.description);
                $("[name=team_id]").val(a.team_id);
                $("[name=id]").val(a.id);
                $('.modal_title').html('Edit Area');
                $(".submit_button").html('Update');
                $(".add-member-popup-wrapper").show(500);
            }

            function dmy(a) {
                if (a == '')
                    return '';
                a = a.split('/');
                b = [];
                b.push(a[2]);
                b.push(a[0]);
                b.push(a[1]);
                b = b.join('-');
                return b;
            }

            $(document).ready(function() {
                $('#team_id').change(function() {
                    var selected_team_name = $(this).find('option:selected').text();
                    $('#team_name').val(selected_team_name);
                });
            });
        </script>
        <style>
            .error {
                color: red;
                font-size: 13px;
                display: block;
                text-align: left;
            }
        </style>
    <?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\petrosafeme\admin\laravel\resources\views/areas/area_list.blade.php ENDPATH**/ ?>