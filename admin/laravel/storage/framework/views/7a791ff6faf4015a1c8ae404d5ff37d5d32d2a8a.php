<link rel="icon" type="image/png" href="<?php echo e(URL::asset('images/fav.png')); ?>"/>
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/bootstrap.css')); ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/font-awesome.min.css')); ?>"/>
<link rel="stylesheet"  href="<?php echo e(URL::asset('css/choices.min.css')); ?>">
<link rel="stylesheet"  href="<?php echo e(URL::asset('css/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet"  href="<?php echo e(URL::asset('css/datatables.min.css')); ?>">
<link rel="stylesheet"  href="<?php echo e(URL::asset('css/select2.min.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('css/style.css?v='.cssVersion())); ?>"/>
<?php echo $__env->yieldPushContent('styles'); ?><?php /**PATH C:\wamp64\www\petrosafme\admin\laravel\resources\views/includes/main_header_assets.blade.php ENDPATH**/ ?>