<!doctype html>
<html lang="en">
<head>
    <title><?php echo e($title ?? 'Petrosafe'); ?></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="theme-color" content="#d5e5f4">
    <?php echo $__env->make('includes.header_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</head>
<body>
    <script>
        var _base_url = '<?php echo e(url('')); ?>';
    </script>
    <div class="wrapper-main pb-0">
        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->make('includes.footer_assets', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
</body>
</html><?php /**PATH C:\wamp64\www\petrosafeme-new\admin\laravel\resources\views/layouts/login.blade.php ENDPATH**/ ?>