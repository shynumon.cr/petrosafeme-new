
<?php $__env->startSection('content'); ?>
    <div class="row login-section m-0">
        <div class="col-lg-8 col-md-6 login-left p-0">
            <h1 class="MyriadPro-Bold text-capital"><span class="MyriadPro-Regular">Welcome to</span><br>PetroSafe</h1>
        </div>
        <div class="col-lg-4 col-md-6 login-right d-flex mx-auto my-auto">

            <form method="POST" action="<?php echo e(route('login')); ?>" id="loginForm">
                <?php echo csrf_field(); ?>
                <div class="login-main mx-auto">
                    <div class="col-sm-12 login-logo">
                        <img src="<?php echo e(URL::asset('images/logo.png')); ?>" class=" mx-auto">
                    </div>
                    <?php if($errors->any()): ?>
                        <div class="alert alert-danger pt5-pr10">
                            <?php echo e(implode('', $errors->all(':message'))); ?>

                        </div>
                    <?php endif; ?>


                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-user"></i>
                        <input class="input-field <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" id="email" placeholder="Email"
                            name="email" value="<?php echo e(old('email')); ?>" type="text" required autocomplete="email"
                            autofocus>
                        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-unlock-alt"></i>
                        <input class="input-field <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="Password"
                            name="password" id="password" type="password" required autocomplete="current-password">
                        <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                    <div class="row remember-set m-0">
                        <div class="col-6 log-remb p-0">
                            <input name="remember" id="remember" class="" type="checkbox"
                                <?php echo e(old('remember') ? 'checked' : ''); ?>>
                            <label for="option-01"> <span></span> &nbsp; Remember Me</label>
                        </div>
                    </div>
                    <div id="msg_error">
                        <p style="color:red;font-size:13px;">&nbsp;</p>
                    </div>
                    <div class="col-sm-12 cm-field-btn p-0 m-0">
                        <input type="submit" class="field-btn CM font-weight-normal" id="button_submit"
                            value=" <?php echo e(__('Login')); ?>">
                        <?php if(Route::has('password.request')): ?>
                            <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
                                <?php echo e(__('Forgot Your Password?')); ?>

                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </form>

            <footer>
                <div class="az-section">
                    <a href="http://azinovatechnologies.com/" target="_blank">
                        <div class="azinova-logo"></div>
                    </a>
                    <p class="no-padding">Powered by :</p>
                    <div class="clear"></div>
                </div>
            </footer>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\petrosafeme-new\admin\laravel\resources\views/auth/login.blade.php ENDPATH**/ ?>