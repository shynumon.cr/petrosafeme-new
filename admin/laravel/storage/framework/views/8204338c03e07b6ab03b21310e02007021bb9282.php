<script type="text/javascript" src="<?php echo e(URL::asset('js/jquery-3.6.0.min.js')); ?>"></script> 
<script type="text/javascript" src="<?php echo e(URL::asset('js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/common.js?v='.jsVersion())); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/sweetalert.min.js')); ?>"></script>
<script type="text/javascript">
 $(document).ready(function() {
    $(".menu-icon").click(function(){
        $(".main-nav").toggle(500);
    });
    $(".top-dropdown-set").click(function(){
        $(".top-dropdown").toggle(500);
    });  
 });   
</script>
<?php echo $__env->yieldPushContent('scripts'); ?><?php /**PATH C:\xampp\htdocs\petrosafme\admin\laravel\resources\views/includes/footer_assets.blade.php ENDPATH**/ ?>