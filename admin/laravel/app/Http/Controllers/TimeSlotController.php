<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Team;
use App\Models\Booking;
use App\Models\TimeSlot;
use App\Models\TimeSlotTeamManagement;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use stdClass;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;

class TimeSlotController extends Controller
{
    public function time_slot(Request $req)
    {
        $team = DB::table('teams')->select('id as team_id', 'team_name', 'contact_no')->where('team_status', 1)->where('deleted_at', null)->get();
        $time_slots = DB::table('time_slots')->select('id as slot_id', 'from_slot', 'to_slot', 'booking_count')->where('slot_status', 1)->where('deleted_at', null)->get();
        $time_slot_team = DB::table('time_slot_team_management as tsm')
            ->select('tsm.id as team_slot_id', 'tsm.slot_date', 'tsm.team_name', 'tsm.team_id', 'tsm.slot_id', 'tsm.booking_count', 'ts.from_slot', 'ts.to_slot')
            ->leftJoin('time_slots as ts', 'ts.id', '=', 'tsm.slot_id')
            ->where('tsm.status', 1)
            ->where('ts.slot_status', 1)
            ->where('ts.deleted_at', null)
            ->where('tsm.deleted_at', null)
            ->get();

        return view('time_slot.time_slot_management', compact('team', 'time_slots', 'time_slot_team'));
    }
    // ************************************************************

    public function get_booking_count_time_slot(Request $request)
    {
        // log::info('Post slot Id'.json_encode($request->all()));
        $slotId = $request->input('slot_id');
        $timeSlot = TimeSlot::find($slotId);
        if ($timeSlot) {
            return response()->json(['booking_count' => $timeSlot->booking_count]);
        }
        return response()->json(['error' => 'Time slot not found'], 404);
    }
    // *******************************************************

    public function team_slot_add_edit(Request $request)
    {
        if (isset($request->team_slot_id) && $request->team_slot_id != '') {
            return $this->edit_team_slot($request);
        }
        // ************************
        log::info('Post team_slot_data' . json_encode($request->all()));
        // Validate the form data

        $nickNames = [
            'slot_date' => 'Slot Date',
            'team_id' => 'Team',
            'slot_id' => 'Time Slot',
            'booking_count' => 'Booking count',
        ];
        $validator = Validator::make($request->all(), [
            'slot_date' => 'required|date',
            'team_id' => 'required',
            'slot_id' => 'required',
            'booking_count' => 'required',
        ], $nickNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        try {
            $existingTeamSlot = TimeSlotTeamManagement::where('team_id', $request->team_id)
                ->where('slot_id', $request->slot_id)
                ->where('slot_date', $request->slot_date)
                ->first();

            if ($existingTeamSlot) {
                return response()->json(['status' => 'error', 'message' => 'Team slot already exists for this team, slot, and date']);
            }
            $team_id = $request->team_id;
            $team = DB::table('teams')->select('team_name')->where('id', $team_id)->first();

            if (!$team) {
                return response()->json(['status' => 'error', 'message' => 'Team not found for this Id']);
            }

            $bookingcount = DB::table('bookings')
                ->select('id')
                ->where('team_id', $request->input('team_id'))
                ->where('slot_id', $request->input('slot_id'))
                ->where('service_date', $request->input('slot_date'))
                ->where('booking_status', 1)
                ->whereNull('deleted_at')
                ->count();

            if ($bookingcount > $request->booking_count) {
                return ['status' => 'error', 'message' => 'Booking Count Exceeded already for this date !'];
            }


            $ts = new TimeSlotTeamManagement();
            $ts->slot_date = $request->slot_date;
            $ts->team_id = $request->team_id;
            $ts->slot_id = $request->slot_id;
            $ts->booking_count = $request->booking_count;
            $ts->team_name = $team->team_name;

            $ts->save();

            $time_slot = DB::table('time_slots as ts')
                ->select('ts.from_slot', 'ts.to_slot')->where('id', $ts->slot_id)
                ->first();
            $event = [
                'team_slot_id' => $ts->id,
                'team_id' => $ts->team_id,
                'slot_id' => $ts->slot_id,
                'team_name' => $ts->team_name,
                'slot_date' => $ts->slot_date,
                'booking_count' => $ts->booking_count,
                'from_slot' => date('h:i A', strtotime($time_slot->from_slot)),
                'to_slot' => date('h:i A', strtotime($time_slot->to_slot)),
            ];
            return ['status' => 'success', 'message' => 'Team slot added successfully !', 'event' => $event];
        } catch (QueryException $e) {
            return ['status' => 'error', 'message' => 'Error occurred while adding team slot!'];
            // return ['status' => 'error', 'message' => $e->getMessage()];
        }
    }
    // ***********************************************
    public function edit_team_slot(Request $request)
    {

        // log::info('post edit team slot' . json_encode($request->all()));
        // return true;
        // ********************************************************** validation
        $nickNames = [
            'slot_date' => 'Slot Date',
            'team_id' => 'Team',
            'slot_id' => 'Time Slot',
            'booking_count' => 'Booking count',
        ];
        $validator = Validator::make($request->all(), [
            'slot_date' => 'required|date',
            'team_id' => 'required',
            'slot_id' => 'required',
            'booking_count' => 'required',
        ], $nickNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        // **********************************************
        $bookingcount = DB::table('bookings')
            ->select('id')
            ->where('team_id', $request->input('team_id'))
            ->where('slot_id', $request->input('slot_id'))
            ->where('service_date', $request->input('slot_date'))
            ->where('booking_status', 1)
            ->whereNull('deleted_at')
            ->count();
        // log::info('booking  count ' . json_encode($bookingcount));

        if ($bookingcount > $request->booking_count) {
            return ['status' => 'error', 'message' => 'Booking Count Exceeded already for this date !'];
        }

        $ts = TimeSlotTeamManagement::find($request->team_slot_id);

        if (!$ts) {
            return response()->json(['status' => 'error', 'message' => 'Team Slot Id Not Found']);
        }
        $existing_teamslot = TimeSlotTeamManagement::where('team_id', $request->team_id)
            ->where('slot_id', $request->slot_id)->where('booking_count', $request->booking_count)
            ->first();

        if ($existing_teamslot) {
            return response()->json(['status' => 'error', 'message' => 'Team slot already exists for this team, slot']);
        }
        $team_id = $request->team_id;
        $team = DB::table('teams')->select('team_name')->where('id', $team_id)->first();

        if (!$team) {
            return response()->json(['status' => 'error', 'message' => 'Team not found for this Id']);
        }

        $ts->slot_date = $request->slot_date;
        $ts->team_id = $request->team_id;
        $ts->slot_id = $request->slot_id;
        $ts->booking_count = $request->booking_count;
        $ts->team_name = $team->team_name;

        $ts->save();
        return ['status' => 'success', 'message' => 'Team slot Updated successfully !'];
    }

    // **************************************************************

}
