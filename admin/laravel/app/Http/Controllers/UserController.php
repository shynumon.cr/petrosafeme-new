<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Role;
use App\Models\UserRoles;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use stdClass;
use Illuminate\Support\Facades\Log ;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function user_view()
    {
       //print_r(auth()->user()->permissions()) ;exit;
        // if(!permissionCheck(getPermission(),'list_users'))
        // return redirect('/');
         $roles = Role::all();
         
        return view('users.users-list',compact('roles'));
    }
    public function list_users(Request $req)
    {
        $p = DB::table('admin')->select('admin.*')
            ->where('admin.status', 'active');

        if (isset($req->from_date) && $req->from_date != '')
            $p = $p->whereRaw('date(admin.created_at) >= "' . $req->from_date . '"');
        if (isset($req->to_date) && $req->to_date != '')
            $p = $p->whereRaw('date(admin.created_at) <= "' . $req->to_date . '"');
        if (isset($req->keywordsearch) && $req->keywordsearch != '')
            $p = $p->where('admin.name', 'like', '%' . $req->keywordsearch . '%');
        if (isset($req->sub_status) && $req->sub_status != '')
            $p = $p->orderBy('admin.name');
        $filtered = $p->count();
        $p = $p->offset($req->start)->limit($req->length);
        $p = $p->orderBy('id', 'DESC')->get();
        $total = DB::table('admin')->where('admin.status', 'active')->count();
        $users = [];
        $j = $req->start;
        foreach ($p as $k => $v) {
            $action = '<div class="tooltip-ation-main">
            <i class="fa fa-cog"></i>
            <div class="tooltip-ation">
                <div class="tp-arrow-back"></div>
                <div class="tp-arrow"></div>
                <ul>';
            // if(permissionCheck(getPermission(),'edit_user'))
        if(auth()->user()->can('edit-user') || auth()->user()->can('all-permissions') || auth()->user()->can('developer-permissions')){ 

            $action .= '<li class="view-action"><a data-url=""><label onclick=edit_user("' . $v->id . '");><i class="fa fa-pencil"></i> Edit</label></a></li>';
            // if(permissionCheck(getPermission(),'delete_user'))
        }
        if(auth()->user()->can('delete-user') || auth()->user()->can('all-permissions') || auth()->user()->can('developer-permissions')){
            $action .= '<li class="edit-action"><a data-url=""><label onclick=delete_user("' . $v->id . '");><i class="fa fa-trash"></i> Delete</label></a></li>';
        }
            $action .= '</ul>
            </div>
        </div>';
            $users[] = [
                'slno' => ($j + 1),
                
                'name' => $v->LoginName,
                'email' => $v->email,
                'username' => $v->username,
                'status' => ucfirst($v->status),
                //'username' => $v->username,
              //  'usertype' => $v->userType,
                'addedtime' => date('d-m-Y h:i A', strtotime($v->created_at)),
                'action' => $action,
                /*'created_date' => date('d-m-Y', strtotime($v->created_at)),
                'created_time' => date('h:i A', strtotime($v->created_at)),
                'updated_date' => date('d-m-Y', strtotime($v->updated_at)),
                'updated_time' => date('h:i A', strtotime($v->updated_at)),
                
                'id' => $v->id,
                'deleted' => $v->status,*/
            ];
            $j++;
        }
        return ['data' => $users, 'draw' => $req->draw, 'recordsTotal' => $total, 'recordsFiltered' => $filtered];
    }
    function get_user(Request $req)
    {
        //$user->roles() log::info('post id '.json_encode($req->all()));
        $p = Admin::with('roles')->Find($req->id);

        if ($p) {
            return ['status' => 'success', 'data' => $p];
        } else {
            return response()->json(['status' => 'error', 'message' => 'No User Found']);
        }
    }
    function add_edit_user(Request $req)
    {
        if (isset($req->id) && $req->id != '') {
            return $this->edit_user($req);
        }
        // logs::info('post datas' . json_encode($req->all()));
        // ********************************************************** validation
        $nickNames = [
            //'usertype' => 'User Type',
        ];
        $validator = Validator::make($req->all(), [
            'LoginName' => 'required',
            'email' => 'email|required|unique:admin',
            'username' => 'required|unique:admin',
            //'password' => 'required',
            //'username' => 'bail|required|unique:admin,username',
          //  'usertype' => 'required',
            // 'role' => 'required',
        ], $nickNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        // **********************************************
        $user = new Admin();
        $role = $req->role;
        $user->created_at = date('Y-m-d H:m:s');
        $user->updated_at = date('Y-m-d H:m:s');
        $user->status = 'active';
        $user->LoginName = $req->LoginName;
        $user->email = $req->email;
        $user->username = $req->username;
        $user->usertype = 'new';
        $user->password = bcrypt($req->password);
        $user->remember_token = Str::random(40);
        $updated_user = $user->save();
        if($updated_user){
            if($role !="")
            {
                $user->roles()->attach($role);
            }
        }
        return ['status' => 'success', 'message' => 'User Added Successfully'];
    }
    public function edit_user(Request $req)
    {
        log::info('post datas' . json_encode($req->all()));
        
        $nickNames = [
        ];
        $validator = Validator::make($req->all(), [
            'LoginName' => 'required',
            'username' => 'required|unique:admin,username,'. $req->id.',id',
            'email' => 'required|unique:admin,email,'. $req->id.',id',
        ], $nickNames);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        $user = Admin::find($req->id);
        $roleCheck = $req->role != null;
         if($roleCheck)
        {
             $role = $req->role;
        }else{
            $role = "";
        }
        $user->updated_at = now();
        $user->LoginName = $req->LoginName;
        $user->username = $req->username;
        $user->email = $req->email;
        if ($req->password !== null) {
            $user->password = $req->password;
        }
        $user->remember_token = Str::random(40);
        $updated_user = $user->save();
        if($updated_user){
            if($role!=""){
                $delete_user = UserRoles::where('admin_id', $req->id)->delete();
                    $user->roles()->attach($role);
            }
        }
        return ['status' => 'success', 'message' => 'User Updated Successfully'];
    }
    function delete_user(Request $req)
    {
        DB::beginTransaction();
        try {
            $p = Admin::Find($req->id);
            $p->status = 'disabled';
            $p->save();
            // Log::admin("User deleted ($req->id)");
            DB::commit();
            return ['status' => 'success', 'message' => 'User Removed Successfully !'];
        } catch (\Throwable $e) {
            DB::rollback();
            return ['status' => 'error', 'message' => 'User Remove Failed !'];
        }
    }
    public function logout(Request $request)
    {
        Auth::logout();
        $base_url =  URL::to('/');
        $urlArray = explode('/',$base_url);
        unset($urlArray[2]);
        $urlArray = array_filter($urlArray); 
        array_pop($urlArray);
        $urlString = implode('/',$urlArray).'/';
        return Redirect::away($urlString);
    }

}

