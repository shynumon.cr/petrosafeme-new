<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use stdClass;
use Illuminate\Support\Facades\Log ;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;
class CategoryController extends Controller
{
    public function index()
    {
        return view('category.categoryList');
    }
    public function list_modules(Request $req)
    {
        $p = DB::table('permission_categories')->select('permission_categories.*');

        /*if (isset($req->from_date) && $req->from_date != '')
            $p = $p->whereRaw('date(permission_categories.created_at) >= "' . $req->from_date . '"');
        if (isset($req->to_date) && $req->to_date != '')
            $p = $p->whereRaw('date(permission_categories.created_at) <= "' . $req->to_date . '"');*/
        if (isset($req->keywordsearch) && $req->keywordsearch != '')
            $p = $p->where('permission_categories.permission_category', 'like', '%' . $req->keywordsearch . '%');
        $filtered = $p->count();
        $p = $p->offset($req->start)->limit($req->length);
        $p = $p->orderBy('id', 'DESC')->get();
        $total = DB::table('permission_categories')->count();
        $categories = [];
        $j = $req->start;
        foreach ($p as $k => $v) {
            $action = '<div class="tooltip-ation-main">
            <i class="fa fa-cog"></i>
            <div class="tooltip-ation">
                <div class="tp-arrow-back"></div>
                <div class="tp-arrow"></div>
                <ul>';
            $action .= '<li class="view-action"><a data-url=""><label onclick=edit_module("' . $v->id . '");><i class="fa fa-pencil"></i> Edit</label></a></li>';
            $action .= '<li class="edit-action"><a data-url=""><label onclick=delete_module("' . $v->id . '");><i class="fa fa-trash"></i> Delete</label></a></li>';
            $action .= '</ul>
            </div>
        </div>';
            $categories[] = [
                'slno' => ($j + 1),
                
                'name' => $v->permission_category,
               // 'slug' => $v->slug,
               // 'addedtime' => date('d-m-Y h:i A', strtotime($v->created_at)),
                'action' => $action,
            ];
            $j++;
        }
        return ['data' => $categories, 'draw' => $req->draw, 'recordsTotal' => $total, 'recordsFiltered' => $filtered];
    }
    function get_module(Request $req)
    {
        $p = Category::Find($req->id);
        if ($p) {
            return ['status' => 'success', 'data' => $p];
        } else {
            return response()->json(['status' => 'error', 'message' => 'No Module Found']);
        }
    }
    function add_edit_module(Request $req)
    {
        if (isset($req->id) && $req->id != '') {
            return $this->edit_module($req);
        }
        $nickNames = [
        ];
        $validator = Validator::make($req->all(), [
            'name' => 'required',
           // 'slug' => 'required|unique:permission_categories'
        ], $nickNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        $p = new stdClass();
        /*$p->created_at = date('Y-m-d H:m:s');
        $p->updated_at = date('Y-m-d H:m:s');*/
        $p->permission_category = $req->name;
        //$p->slug = $req->slug;
        $p = (array) $p;
        $us_id = DB::table('permission_categories')->insertGetId($p);
        return ['status' => 'success', 'message' => 'Module Added Successfully'];
    }
    public function edit_module(Request $req)
    {
        log::info('post datas' . json_encode($req->all()));
        $nickNames = [
        ];
        $validator = Validator::make($req->all(), [
            'name' => 'required',
           // 'slug' => 'required|unique:permission_categories,slug,'. $req->id.',id',
        ], $nickNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        $category = Category::find($req->id);
        $category->permission_category = $req->name;
        $category->save();
        return ['status' => 'success', 'message' => 'Module Updated Successfully'];
    }
    function delete_module(Request $req)
    {
        DB::beginTransaction();
        try {
            Category::where('id', $req->id)
            ->delete();
            return ['status' => 'success', 'message' => 'Module Removed Successfully!'];
        } catch (\Throwable $e) {
            DB::rollback();
            return ['status' => 'error', 'message' => 'Module Remove Failed !'];
        }
    }
}
