<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;
use App\Models\Team;
use App\Models\CustomerDocument;
use App\Models\TimeSlotTeamManagement;
use App\Models\Booking;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use stdClass;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BookingController extends Controller
{
    public function booking_list()
    {
        $category = Booking::select('id', 'team_id', 'area_id', 'team_name')->where('booking_status', 1)->get();
        // print_r($customers);
        // die();
        return view('bookings.booking_list');
    }
    // ***********************************************************************************************
    public function new_booking()
    {
        $team = DB::table('teams')->select('id as team_id', 'team_name', 'contact_no')->where('team_status', 1)->where('deleted_at', null)->get();
        $time_slots = DB::table('time_slots')->select('id as slot_id', 'from_slot', 'to_slot', 'booking_count')->where('slot_status', 1)->where('deleted_at', null)->get();

        return view('bookings.new_booking', compact('team', 'time_slots'));
    }
    // ******************************************************************************
    public function list_Booking(Request $req)
    {
        // Log::info('Post Data:'.json_encode($req->all()));
        $p = DB::table('bookings');
        if (isset($req->from_date) && $req->from_date != '')
            $p = $p->whereRaw('date(created_at) >= "' . $req->from_date . '"');
        if (isset($req->to_date) && $req->to_date != '')
            $p = $p->whereRaw('date(created_at) <= "' . $req->to_date . '"');
        if (isset($req->keywordsearch) && $req->keywordsearch != '')
            $p = $p->where('team_name', 'like', '%' . $req->keywordsearch . '%');
        if (isset($req->sub_status) && $req->sub_status != '') {
            if ($req->sub_status == 'name.asc') {
                $p = $p->orderBy('id', 'ASC');
            } else if ($req->sub_status == 'name.desc') {
                $p = $p->orderBy('id', 'DESC');
            } else if ($req->sub_status == 'created_at.asc') {
                $p = $p->orderBy('created_at', 'ASC');
            } else {
                $p = $p->orderBy('created_at', 'DESC');
            }
        } else {
            $p = $p->orderBy('id', 'DESC');
        }

        $filtered = $p->count();
        $p = $p->offset($req->start)->limit($req->length);
        $p = $p->get();
        $total = DB::table('bookings')->count();
        $products = [];
        $j = $req->start;
        foreach ($p as $k => $v) {
            $action = '<div class="tooltip-ation-main">
            <i class="fa fa-cog"></i>
            <div class="tooltip-ation">
                <div class="tp-arrow-back"></div>
                <div class="tp-arrow"></div>
                <ul>';
            $action .= '<li class="view-action"><a data-url=""><label onclick=view_booking("' . $v->id . '");><i class="fa fa-eye"></i> View</label></a></li>';
            $action .= '<li class="view-action"><a data-url=""><label onclick=edit_booking("' . $v->id . '");><i class="fa fa-pencil"></i> Edit</label></a></li>';
            $action .= '</ul>
            </div>
        </div>';
            $products[] = [
                'slno' => ($j + 1),
                'id' => $v->id,
                'reference_id ' => $v->reference_id,
                'team_id' => $v->team_id,
                'area_id' => $v->area_id,
                'area_name' => $v->area_name,
                'team_name' => $v->team_name,
                'from_slot' => $v->from_slot,
                'to_slot ' => $v->to_slot,
                'service_date  ' => $v->service_date,
                'booking_count' => $v->booking_count,
                'booking_status' => $v->booking_status,
                'booking_type' => $v->booking_type,
                'created_date' => date('d-m-Y', strtotime($v->created_at)),
                'created_time' => date('h:i A', strtotime($v->created_at)),
                'updated_date' => date('d-m-Y', strtotime($v->updated_at)),
                'updated_time' => date('h:i A', strtotime($v->updated_at)),
                'action' => $action,
            ];
            $j++;
        }
        return ['data' => $products, 'draw' => $req->draw, 'recordsTotal' => $total, 'recordsFiltered' => $filtered];
    }
    // *********************************************************************************
    public function add_edit_booking(Request $request)
    {
        log::info('post booking ' . json_encode($request->all()));
        if (isset($request->id) && $request->id != '') {
            return $this->edit_Booking($request);
        }

        /************************************************* */ // validate
        $niceNames = [
            'customer_name' => 'Customer Name',
            'booking_type' => 'Booking Type',
        ];

        $rules = [
            'customer_name' => 'required',
            'booking_type' => 'required',
            'mob_no' => 'required',
            'address' => 'required',
            'flat_no' => 'required',
            'building_name' => 'required',
            'whatsapp_no' => 'required',
            'service_date' => 'required',
            'team' => 'required',
            'area' => 'required',
            'slot_id' => 'required',
            'comments' => 'required',
        ];

        $booking_type = $request->input('booking_type');

        if ($booking_type == 'CON') {
            $rules += [
                'gas_available' => 'required',
                'active_dewa_connection' => 'required',
                'payment_mode' => 'required',
                'eid_front' => 'required|file',
                'eid_back' => 'required|file',
                'tenancy' => 'required|file',
                'meter_picture' => 'required|file',
                'gas_cooker_photo' => 'required|file',
            ];
        } elseif ($booking_type == 'DIS') {
            $rules += [
                'eid_front' => 'required|file',
                'eid_back' => 'required|file',
                'gas_contract_type' => 'required',
                'availability' => 'required',
                'gas_contract_image' => 'required|file',
            ];
        } elseif ($booking_type == 'SHIF') {
            $rules += [
                'new_building_name' => 'required',
                'new_flat_no' => 'required',
                'shifting_to' => 'required',
                'eid_front' => 'required|file',
                'eid_back' => 'required|file',
                'tenancy' => 'required|file',
                'gas_available' => 'required',
                'active_dewa_connection' => 'required',
                'payment_mode' => 'required',
            ];
        }

        $validator = Validator::make($request->all(), $rules, [], $niceNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }

        /************************************************* */

        $main_book_count = DB::table('time_slots')
            ->select('booking_count')
            ->where('id', $request->input('slot_id'))
            ->where('slot_status', 1)
            ->whereNull('deleted_at')
            ->value('booking_count');
        log::info('time slot count ' . json_encode($main_book_count));

        $blocked_count = DB::table('time_slot_team_management')
            ->select('booking_count')
            ->where('team_id', $request->input('team'))
            ->where('slot_id', $request->input('slot_id'))
            ->where('slot_date', $request->input('service_date'))
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->value('booking_count');
        log::info('blocked count ' . json_encode($blocked_count));

        $bookingcount = DB::table('bookings')
            ->select('id')
            ->where('team_id', $request->input('team'))
            ->where('slot_id', $request->input('slot_id'))
            ->where('service_date', $request->input('service_date'))
            ->where('booking_status', 1)
            ->whereNull('deleted_at')
            ->count();
        log::info('booking  count ' . json_encode($bookingcount));

        $area = Area::select('area_name')->where('id', $request->area)->first();
        $team = Team::select('team_name')->where('id', $request->team)->first();

        if (!is_null($blocked_count)) {

            if ($bookingcount < $blocked_count) {
                $b = new Booking();
                $last_booking = Booking::latest()->first();
                $new_id = $last_booking ? $last_booking->id + 1 : 1;
                $reference_id = 'PETRO-' . str_pad($new_id, 7, '0', STR_PAD_LEFT);
                $b->reference_id = $reference_id;
                $b->team_id = $request->team;
                $b->slot_id = $request->slot_id;
                $b->area_id = $request->area;
                $b->area_name = $area->area_name;
                $b->team_name = $team->team_name;
                $b->customer_name = $request->customer_name;
                $b->service_date = $request->service_date;
                $b->booking_count = $main_book_count; //not using
                $b->booking_type = $request->booking_type;
                $b->comments = $request->comments;
                $b->booking_status = 1;
                $b->gas_available = $request->gas_available ?: null;
                $b->active_dewa_connection = $request->active_dewa_connection ?: 'NO';
                $b->payment_mode = $request->payment_mode ?: 'NO';
                $b->payment_status = $request->payment_status ?: 'NO';
                $b->building_name = $request->building_name ?: null;
                $b->flat_no = $request->flat_no ?: null;
                $b->mob_no = $request->mob_no ?: null;
                $b->whatsapp_no = $request->whatsapp_no ?: null;
                $b->address = $request->address ?: null;
                $b->new_building_name = $request->new_building_name ?: null;
                $b->shifting_to = $request->shifting_to ?: null;
                $b->availability = $request->availability ?: null;
                $b->gas_contract_type = $request->gas_contract_type ?: null;
                $b->new_flat_no = $request->new_flat_no ?: null;
                $b->created_at = date('Y-m-d H:m:s');
                $b->updated_at = null;
                $b->save();

                $cd = new CustomerDocument();

                if ($request->hasFile('eid_front')) {
                    $file_name = 'eid_front' . time() . '.' . $request->eid_front->extension();
                    $request->eid_front->move('document_images', $file_name);
                    $cd->eid_front = $file_name;
                }
                if ($request->hasFile('eid_back')) {
                    $file_name = 'eid_back' . time() . '.' . $request->eid_back->extension();
                    $request->eid_back->move('document_images', $file_name);
                    $cd->eid_back = $file_name;
                }
                if ($request->hasFile('tenancy')) {
                    $file_name = 'tenancy' . time() . '.' . $request->tenancy->extension();
                    $request->tenancy->move('document_images', $file_name);
                    $cd->tenancy = $file_name;
                }
                if ($request->hasFile('meter_picture')) {
                    $file_name = 'meter_picture' . time() . '.' . $request->meter_picture->extension();
                    $request->meter_picture->move('document_images', $file_name);
                    $cd->meter_picture = $file_name;
                }
                if ($request->hasFile('gas_cooker_photo')) {
                    $file_name = 'gas_cooker_photo' . time() . '.' . $request->gas_cooker_photo->extension();
                    $request->gas_cooker_photo->move('document_images', $file_name);
                    $cd->gas_cooker_photo = $file_name;
                }
                if ($request->hasFile('gas_contract_image')) {
                    $file_name = 'gas_contract_image' . time() . '.' . $request->gas_contract_image->extension();
                    $request->gas_contract_image->move('document_images', $file_name);
                    $cd->gas_contract_image = $file_name;
                }

                $cd->booking_id = $b->id;
                $cd->created_at = date('Y-m-d H:m:s');
                $cd->updated_at = null;
                $cd->save();
            } else {
                return ['status' => 'error', 'message' => 'Booking Count Limit Reached For this Day'];
            }
        } else {
            if ($bookingcount < $main_book_count) {

                $b = new Booking();
                $last_booking = Booking::latest()->first();
                $new_id = $last_booking ? $last_booking->id + 1 : 1;
                $reference_id = 'PETRO-' . str_pad($new_id, 7, '0', STR_PAD_LEFT);
                $b->reference_id = $reference_id;
                $b->team_id = $request->team;
                $b->slot_id = $request->slot_id;
                $b->area_id = $request->area;
                $b->area_name = $area->area_name;
                $b->team_name = $team->team_name;
                $b->customer_name = $request->customer_name;
                $b->service_date = $request->service_date;
                $b->booking_count = $main_book_count; //not using
                $b->booking_type = $request->booking_type;
                $b->comments = $request->comments;
                $b->booking_status = 1;
                $b->gas_available = $request->gas_available ?: null;
                $b->active_dewa_connection = $request->active_dewa_connection ?: 'NO';
                $b->payment_mode = $request->payment_mode ?: 'NO';
                $b->payment_status = $request->payment_status ?: 'NO';
                $b->building_name = $request->building_name ?: null;
                $b->flat_no = $request->flat_no ?: null;
                $b->mob_no = $request->mob_no ?: null;
                $b->whatsapp_no = $request->whatsapp_no ?: null;
                $b->address = $request->address ?: null;
                $b->new_building_name = $request->new_building_name ?: null;
                $b->shifting_to = $request->shifting_to ?: null;
                $b->availability = $request->availability ?: null;
                $b->gas_contract_type = $request->gas_contract_type ?: null;
                $b->new_flat_no = $request->new_flat_no ?: null;
                $b->created_at = date('Y-m-d H:m:s');
                $b->updated_at = null;
                $b->save();

                $cd = new CustomerDocument();

                if ($request->hasFile('eid_front')) {
                    $file_name = 'eid_front' . time() . '.' . $request->eid_front->extension();
                    $request->eid_front->move('document_images', $file_name);
                    $cd->eid_front = $file_name;
                }
                if ($request->hasFile('eid_back')) {
                    $file_name = 'eid_back' . time() . '.' . $request->eid_back->extension();
                    $request->eid_back->move('document_images', $file_name);
                    $cd->eid_back = $file_name;
                }
                if ($request->hasFile('tenancy')) {
                    $file_name = 'tenancy' . time() . '.' . $request->tenancy->extension();
                    $request->tenancy->move('document_images', $file_name);
                    $cd->tenancy = $file_name;
                }
                if ($request->hasFile('meter_picture')) {
                    $file_name = 'meter_picture' . time() . '.' . $request->meter_picture->extension();
                    $request->meter_picture->move('document_images', $file_name);
                    $cd->meter_picture = $file_name;
                }
                if ($request->hasFile('gas_cooker_photo')) {
                    $file_name = 'gas_cooker_photo' . time() . '.' . $request->gas_cooker_photo->extension();
                    $request->gas_cooker_photo->move('document_images', $file_name);
                    $cd->gas_cooker_photo = $file_name;
                }
                if ($request->hasFile('gas_contract_image')) {
                    $file_name = 'gas_contract_image' . time() . '.' . $request->gas_contract_image->extension();
                    $request->gas_contract_image->move('document_images', $file_name);
                    $cd->gas_contract_image = $file_name;
                }

                $cd->booking_id = $b->id;
                $cd->created_at = date('Y-m-d H:m:s');
                $cd->updated_at = null;
                $cd->save();
            } else {
                return ['status' => 'error', 'message' => 'Booking Count Limit Reached For this Day'];
            }
        }

        $id = DB::table('bookings')->insertGetId($b->toArray());
        if ($id) {
            return ['status' => 'success', 'message' => 'Booking Added Successfully !'];
        } else {
            return ['status' => 'error', 'message' => 'Booking Adding Failed !'];
        }
    }
    // *****************************************************************************
    function get_Booking(Request $req)
    {
        // log::info('post id '.json_encode($req->all()));
        $p = Booking::Find($req->id);
        if ($p) {
            return ['status' => 'success', 'data' => $p];
        } else {
            return response()->json(['status' => 'error', 'message' => 'No Booking Id Found']);
        }
    }
    function get_area_by_team(Request $req)
    {
        // log::info('post id '.json_encode($req->all()));
        $areas = Area::where('team_id', $req->id)->where('area_status', 1)->where('deleted_at', null)->get();
        if ($areas->count() > 0) {
            return response()->json(['status' => 'success', 'data' => $areas]);
        } else {
            return response()->json(['status' => 'error', 'message' => 'No Areas Found']);
        }
    }
    // ********************************************************************************

    public function edit_Booking(Request $req)
    {
        /************************************************* */ // validate
        $niceNames = [
            'title' => 'Booking Title',
            'arabicTitle' => 'Arabic Title',
        ];
        $validator = Validator::make($req->all(), [
            'title' => 'bail|required|unique:Bookings,title,' . $req->id,
            'arabicTitle' => 'bail|required|unique:Bookings,arabicTitle,' . $req->id,
            'BookingMessage' => 'bail|required',
            'arabicBookingMessage' => 'bail|required',
            'category_name' => 'bail|required',
            'priority_name' => 'bail|required',
            'customer_name' => 'bail|required',
        ], [], $niceNames);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->first()]);
        }
        /************************************************* */
        $p = Booking::Find($req->id);

        if (!$p) {
            return response()->json(['status' => 'error', 'message' => 'Booking not found']);
        }

        $p->title = $req->title;
        $p->arabicTitle = $req->arabicTitle;
        $p->BookingMessage = $req->BookingMessage;
        $p->arabicBookingMessage = $req->arabicBookingMessage;
        $p->BookingCategoryId = $req->category_name;
        $p->priorityId = $req->priority_name;
        $p->customerId = $req->customer_name;
        $p->updated_at = now();
        $p->save();
        return ['status' => 'success', 'message' => 'Booking Updated Successfully'];
    }
    // ************************************************************************************
    function delete_Booking_new(Request $req)
    {
        $p = Booking::Find($req->id);
        $p->status = 0;
        $p->save();
        return ['status' => 'success', 'message' => 'Booking Removed Successfully'];
    }
    // ************************************************************************************

    public function view_Booking_details($id)
    {
        try {
            // log::info('post id ' . json_encode($id));

            $query = Booking::leftjoin('Booking_categories', 'Bookings.BookingCategoryId', '=', 'Booking_categories.id')
                ->leftjoin('Booking_priorities', 'Bookings.priorityId', '=', 'Booking_priorities.id')
                ->leftjoin('customers', 'Bookings.customerId', '=', 'customers.id')
                ->select(
                    'Bookings.*',
                    'Booking_categories.categoryName',
                    'Booking_categories.categoryArabicName',
                    'Booking_priorities.priorityName',
                    'Booking_priorities.priorityArabicName',
                    'customers.id as customer_id',
                    'customers.name',
                    'customers.nameArabic',
                    'customers.mobileNumber',
                    'customers.emailId',
                    'customers.image',
                )
                ->where('Bookings.id', $id);
            // ->where('Booking_categories.status', 1)
            // ->where('Booking_priorities.status', 1)
            // ->where('customers.status', 1);

            $Bookingdetails = $query->first();

            if ($Bookingdetails) {
                // Log::info("Result: " . json_encode($Bookingdetails));
                $comments = Booking::where('BookingId', $id)->where('commentStatus', 1)->get();
                return view('Booking_list.Booking_details', compact('Bookingdetails', 'comments'));
            } else {
                Log::info("No Booking details found for ID: " . $id);

                return response()->json(['status' => 'error', 'message' => 'No Booking details found.']);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => 'An error occurred.']);
        }
    }

    // ************************************************************************************************

    public function updateBookingstatus(Request $request, $id)
    {
        // log::info('post status'.json_encode($request->all()));
        $Booking = Booking::find($id);

        if (!$Booking) {
            return response()->json(['status' => 'error', 'message' => 'Booking not found']);
        }

        $Booking->BookingStatus = $request->input('status');
        $Booking->save();

        return response()->json(['status' => 'success', 'message' => 'Booking status updated successfully']);
    }


    // *******************************************************************************************************




}
