<?php
  
namespace App\Models;
  
  use App\Traits\HasPermissionsTrait;
  //use App\Permissions\HasPermissionsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
/*
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;*/
  
class Admin extends Authenticatable
{
    //use HasFactory, Notifiable, HasRoles;
    use HasApiTokens,
        HasFactory,
        Notifiable,
        HasPermissionsTrait;
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "admin";
    protected $fillable = [
        'LoginName',
        'email',
        'password',
        'odoo_id',
        'username',
    ];
  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
  
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /*public function getAuthPassword()
    {
      return $this->Password;
    }*/

    /*public function user_role() {

       return $this->belongsToMany(Role::class,'users_roles');
           
    }*/
}