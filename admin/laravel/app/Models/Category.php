<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'permission_categories';
	protected $fillable = [
        'id','permission_category'
    ];
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function permission()
    {
        return $this->hasMany(permission::class,'category');
    }
}
