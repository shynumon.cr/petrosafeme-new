@extends('layouts.main')
@section('content')
    {{-- <?php
    $per = getPermission();
    ?> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="add-member-popup-wrapper">
   <div class="add-member-popup-section d-flex">
      <div class="add-member-popup-main col-sm-5 mx-auto">
         <div class="close-btn"><img src="{{ URL::asset('images/white-close.png') }}" alt=""></div>
         <h5 class="MyriadPro-Bold modal_title">Add Role</h5>
         <div class="col-12 step-cont" id="cont1">
            <form id="role_form"  method="post">
               <div class="row m-0">
                  <div class="col-sm-6 cm-field-main cm-field">
                     <p>Name<span class="text-danger">*</span></p>
                     <input class="input-field" placeholder="Role Name" name="name" type="text" id="name">
                     <input type="hidden" name="id">
                  </div>
                    <div class="col-sm-6 cm-field-main cm-field">
                        <p>Slug<span class="text-danger">*</span></p>
                         <input class="input-field" placeholder="Slug" name="slug" type="text" id="slug">
                    </div>
                        
                  
                  <div class="col-sm-12">
                    <div class="row">
                       <div class="col-md-12">
                          @if(count($all_category_permission)>0) 
                          <div class="table-responsive">
                             <table class="table card-table table-vcenter text-nowrap  align-items-center">
                                <thead class=" text-white">
                                   <tr>
                                      <th class="text-center">Category</th>
                                      <th class=" text-center">Permissions</th></tr>
                                   </thead>
                                   <tbody>
                                      @foreach($all_category_permission as $key => $category)

                                      <tr>
                                         <td>{{$key}}</td>
                                         <td>
                                            <?php $checkbox_count = "0"; ?>
                                            @foreach($category as $k => $permission)

                                            <?php 
                                            $checkbox_count++;
                                            if($k != "developer-permissions"){?>

                                               <label class="custom-control1 custom-checkbox" style="display: inline-grid;
    margin-right: 30px;">
                                                  <input type="checkbox" class="custom-control-input <?php if($key=="Document Management"){echo "pdf_doc";}else if($key=="Employee Management"){echo "employee";}else if($key=="Frontend Header Footer Template Management"){echo "header_footer";}else if($key=="Softeware Settings"){echo "settings";}else if($key=="User Management"){echo "user";} ?>" name="permissions[]" value="{{$k}}" id="<?php if($k=="list-pdf-document"){echo "pdf_doc_list";}else if($k=="list-employee"){echo "employee_list";}else if($k=="list-header-footer-template"){echo "header_footer_list";}else if($k=="view-settings"){echo "settings_list";}else if($k=="list-user"){echo "user_list";} ?>" >
                                                  <span class="custom-control-label">{{$permission}}</span>
                                               </label>
                                               <?php 
                                               if($checkbox_count>4)
                                                  { 
                                                     echo "<br/>";
                                                     $checkbox_count = 0;
                                                  } ?>

                                            <?php } ?>


                                         @endforeach</td>
                                      </tr>
                                      @endforeach
                                   </tbody>
                                </table>
                             </div>
                             @else
                             <p>No Permission Available</p>
                             @endif

                       </div>
                    </div>
                  </div>
                  <div class="col-sm-12 cm-field-btn">
                     <button type="submit" class="submit_buttom field-btn CM font-weight-normal w-auto">Save</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>






    <div class="common-popup-wrapper" id="delete_popup" style="display:none;">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main mx-auto col-lg-4 col-md-6 col-sm-6">
                <div class="close-btn"><img src="{{ URL::asset('images/white-close.png') }}" alt=""></div>
                <div class="common-popup-icon m-auto"><img src="{{ URL::asset('images/alert.gif') }}" alt=""></div>
                <h5 class="MyriadPro-Bold text-center">Are you sure to delete</h5>
                <div class="common-popup-text">
                    <div class="col-12 cm-field-main cm-field p-0 pr-0 text-center mt-4">
                        <input type="button" class="field-btn CM font-weight-normal w-auto Danger" value="Delete">
                        <input type="button" class="field-btn font-weight-normal w-auto" value="Cancel">
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Pop Up -->

    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">Roles List</h4>
                </li>
            </ul>
        </div>
        <!--page-title-main end-->

        <div class="col-12 cm-content-main">
            <div class="col-12 table-main">
                <div class="col-12 table-filter">
                    <ul>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Search Keyword</p>
                                <input class="input-field" placeholder="Search Here" id="search" type="text">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Start Date</p>
                                <input class="form-control input-field calendar" placeholder="Start Date" id="start_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>End Date</p>
                                <input class="form-control input-field calendar" placeholder="End Date" id="end_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="search()"><i class="fa fa-search"></i>Search</span>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Sort by</p>
                                <select name="" id="sub_status" class="time-slot" onchange="sortData()";>
                                    <option value="" data-url="">Select from list</option>
                                    <option value="name" data-url="">Name</option>
                                </select>

                            </div>
                        </li>

                        <li>
                            <div class="cm-field-main m-0">
                                <p>Show By</p>
                                <select name="show_by" id="show_by">
                                    @foreach ([10, 25, 50, 100] as $it)
                                        <option {{ $it == 50 ? 'selected' : '' }} value="{{ $it }}">
                                            {{ $it }} entries</option>
                                    @endforeach
                                </select>
                            </div>
                        </li>
                       
                        <li class="float-right text-right width-auto">
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="add_product();"><i class="fa fa-plus"></i>New</span>
                            </div>
                        </li>
                        {{-- @endif --}}
                    </ul>
                </div>

                <table id="roles_table" class="table table-sm table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>
                                Sl. No.
                            </th>
                            <th>
                                Role Name
                            </th>
                            <th>
                                Slug
                            </th>
                           <th class="action">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!--cm-content-main end-->
        </div>
        <!--cm-content-section end-->
    @endsection
    @push('scripts')
        <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var studentTable = '';
            $(document).ready(function() {
                $("#start_date,#end_date").datepicker({
                    autoclose: true
                });
                studentTable = $('#roles_table').DataTable({
                    'bFilter': false,
                    'bLengthChange': false,
                    'pageLength': 50,
                    'processing': true,
                    'serverSide': true,
                    'bSort': false,
                    'serverMethod': 'post',
                    //  "bDestroy": true,
                    'ajax': {
                        'url': '{{ Url('list-roles') }}',
                        'data': function(data) {
                            data.from_date = dmy($('#start_date').val());
                            data.to_date = dmy($('#end_date').val());
                            data.keywordsearch = $('#search').val();
                            data.sub_status = $('#sub_status').val();
                        },
                        "complete": function(json, type) {
                            var _resp = $.parseJSON(json.responseText);
                            console.log(_resp);
                        }
                    },
                    'columns': [{
                            data: 'slno'
                        },
                        {
                            data: 'name'
                        },
                        {
                            data: 'slug'
                        },
                        {
                            data: 'action'
                        },
                    ]
                    
                });











                $("#role_form").validate({
                    ignore: [],
                    rules: {
                        name: {
                            required: true
                        },
                        slug: {
                            required: true
                        }
                    },
                    errorPlacement: function(err, ele) {
                        err.insertAfter(ele);
                    },
                    submitHandler: function(form, element) {
                        $(".submit_buttom").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop(
                            'disabled', true);
                        fm = new FormData(form);
                        $.ajax({
                            url: '{{ Url('add-edit-role') }}',
                            type: 'post',
                            data: fm,
                            cache: false,
                            processData: false,
                            contentType: false,
                            success: function(data) {
                                console.log(data);
                                if (data.status == 'success') {
                                    $("#role_form")[0].reset();
                                    $(".add-member-popup-wrapper").hide(500);
                                    studentTable.draw();
                                    swal("Success!", data.message, "success");
                                    $(".submit_buttom").html('Save').prop('disabled', false);
                                } else {
                                    $(".add-member-popup-wrapper").hide(500);
                                    $(".submit_buttom").html('Save').prop('disabled', false);
                                    swal("Error!", data.message, "error");
                                }
                            },
                            error: function(xhr, status, error) {
                                console.error(xhr.responseText);
                            }
                        });
                    }
                })

                $('#show_by').change(function() {
                    studentTable.page.len($(this).val()).draw();
                })
            });

            function add_product() {
                $('#passwordsec').show();
                $("#password").rules('add', {
                    required: true
                })
                $(".submit_buttom").html('Save');
                $('[name=id]').val('');
                $('.modal_title').html('Add Role');
                $("#role_form")[0].reset();
                $(".add-member-popup-wrapper").show(500);
            }


            function delete_role(id) {
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this Role!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: '{{ Url('delete-role') }}',
                                type: 'post',
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    if (data.status == 'success') {
                                        studentTable.draw();
                                        swal("Success!", data.message, "success");
                                    } else {
                                        swal("Error!", data.message, "error");
                                    }
                                }

                            })
                        }
                    });
            }

            function search() {
                studentTable.draw();
            }

            function sortData() {
                studentTable.draw();
            }

            function edit_role(id) {
                $.ajax({
                    url: '{{ Url('get-roles') }}',
                    type: 'post',
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data.status == 'success') {
                            $('#pass_fields').hide();
                            //$('#change_pass_fields').show();
                            put_fields(data.data);
                        } else {
                            swal("Error!", data.message, "error");
                        }
                    }
                })
            }
/*function myFunction(item, index) {
  text += index + ": " + item + "<br>"; 
}*/
            function put_fields(a) {
             /*   console.log(a);
                let text = "";*/
if(a.permissions)
{
    /*for (var i = 0, max = a.permissions.length; i < max; i++){*/
       // alert(a.permissions[i].name);
        $('.table').find(':checkbox[name="permissions[]"]').each(function (){

           // $(this).attr("checked", true);
            for (var i = 0, max = a.permissions.length; i < max; i++){
                //alert(a.permissions[i].name);
                if($(this).val() === a.permissions[i].slug){
                    $(this).attr("checked", true);
                    
                }
                /*$(this).attr("checked", $.inArray(parseInt($(this).val()), a.permissions[i].name) == -1 ? false : true );*/
            }
        });
    //}
    /*initValues=a.permissions;//[1,2,3];
initValues.forEach(myFunction);
alert(text);*/
}
/*$('.table').find(':checkbox[name="permissions[]"]').each(function (){
        $(this).prop("checked", $.inArray(parseInt($(this).val()), initValues.name) == -1 ? false : true );
    });
}*/

                $("[name=name]").val(a.name);
                $("[name=slug]").val(a.slug);
                $("[name=id]").val(a.id);
                $('.modal_title').html('Edit Role');
                $(".submit_buttom").html('Update');
                $(".add-member-popup-wrapper").show(500);
            }

            function dmy(a) {
                if (a == '')
                    return '';
                a = a.split('/');
                b = [];
                b.push(a[2]);
                b.push(a[0]);
                b.push(a[1]);
                b = b.join('-');
                return b;
            }
        </script>
        <style>
            .error {
                color: red;
                font-size: 13px;
                display: block;
                text-align: left;
            }
        </style>
    @endpush
