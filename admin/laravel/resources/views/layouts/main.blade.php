<!doctype html>
<html lang="en">
<head>
    <title>Petrosafe - @yield('title','Admin')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="theme-color" content="#d5e5f4">
    @include('includes.main_header_assets')
</head>
<body>
    <div class="wrapper-main">
        @include('includes.header')
        <style>
        body .select2-container.select2-container--open {
            z-index: 99999999 !important;
        }
        </style>
        @yield('content')
        @include('includes.footer')
        @include('includes.main_footer_assets')
</div>
</body>
</html>
