@extends('layouts.login')
@section('content')
<div class="row login-section m-0">
        <div class="col-lg-8 col-md-6 login-left p-0">
            <h1 class="MyriadPro-Bold text-capital"><span class="MyriadPro-Regular">Welcome to</span><br>PetroSafe</h1>
        </div>
        <div class="col-lg-4 col-md-6 login-right d-flex mx-auto my-auto">
            <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="login-main mx-auto">
                    <div class="col-sm-12 login-logo">
                        <img src="{{ URL::asset('images/logo.png') }}" class=" mx-auto">
                    </div>
                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-user"></i>
                                <input id="LoginName" type="text" class="input-field  @error('LoginName') is-invalid @enderror" name="LoginName" value="{{ old('LoginName') }}" placeholder="Name" required autocomplete="LoginName" autofocus>

                                @error('LoginName')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                            </div>
                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-envelope"></i>
                                <input id="email" type="email" class="input-field  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                            </div>
                            <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-user"></i>
                                <input id="username" type="text" class="input-field  @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Username" required autocomplete="username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                            </div>
                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-unlock-alt"></i>
                                <input id="password" type="password" class="input-field  @error('password') is-invalid @enderror" name="password" required placeholder="Password"autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
                    <div class="col-sm-12 cm-field-main position-relative p-0">
                        <i class="fa fa-unlock-alt"></i>
                                <input id="password-confirm" type="password" class="input-field " name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">
                            </div>
                       
                            <div class="col-sm-12 cm-field-btn p-0 m-0">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        
                    </form>
            <!-- <footer>
                <div class="az-section">
                    <a href="http://azinovatechnologies.com/" target="_blank">
                        <div class="azinova-logo"></div>
                    </a>
                    <p class="no-padding">Powered by :</p>
                    <div class="clear"></div>
                </div>
            </footer> -->
        </div>
    </div>
@endsection