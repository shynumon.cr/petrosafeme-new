@extends('layouts.main')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="common-popup-wrapper" id="delete_popup" style="display:none;">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main mx-auto col-lg-4 col-md-6 col-sm-6">
                <div class="close-btn"><img src="{{ URL::asset('images/white-close.png') }}" alt=""></div>
                <div class="common-popup-icon m-auto"><img src="{{ URL::asset('images/alert.gif') }}" alt=""></div>
                <h5 class="MyriadPro-Bold text-center">Are you sure to delete</h5>
                <div class="common-popup-text">
                    <div class="col-12 cm-field-main cm-field p-0 pr-0 text-center mt-4">
                        <input type="button" class="field-btn CM font-weight-normal w-auto Danger" value="Delete">
                        <input type="button" class="field-btn font-weight-normal w-auto" value="Cancel">
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Pop Up -->

    <div class="row cm-content-section m-0">
        <div class="col-4 page-title-main">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">Change Password</h4>
                </li>
            </ul>
        </div>
        <div class="col-4">
        </div>
        <div class="col-4 page-title-main">
            <ul>
                <li class="float-right">Last Login: <span
                        class="text-blue bold">{{ date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime)) }}</span>
                </li>
            </ul>
        </div>
        <!--page-title-main end-->
        <div class="col-xl-4 col-lg-4 col-md-3 col-sm-2"></div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 cm-content-main p-4">
            <form id="cpassword_form" class="row" method="post">
                <div class="col-12">
                    <div class="form-row">
                        <div class="col-12 cm-field-main cm-field">
                            <p>Current Password</p>
                            <input class="input-field" name="current_password" id="current_password" type="password"
                                value="">
                        </div>
                        <div class="col-12 cm-field-main cm-field">
                            <p>New Password</p>
                            <div class="input-group">
                                <input type="password" class="form-control" name="password" id="password" value="">
                                <div class="input-group-append">
                                    <span class="input-group-text btn" onclick="togglePasswordView()">
                                        <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                        <i class="fa fa-eye" aria-hidden="true" style="display: none;"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 cm-field-main cm-field">
                            <p>Confirm New Password</p>
                            <input class="input-field" name="password_confirmation" id="password_confirmation"
                                type="password" value="">
                        </div>
                        <div class="form-row">
                            <div class="col-12 cm-field-btn">
                                <button class="submit_buttom field-btn CM font-weight-normal w-auto"
                                    onSumbit="submit()">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--cm-content-main end-->
        </div>
        <div class="col-xl-4 col-lg-4 col-md-3 col-sm-2"></div>
        <!--cm-content-section end-->
    @endsection
    @push('scripts')
        <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function togglePasswordView() {
                if ($('#password').prop('type') == 'password') {
                    $(".fa-eye-slash").hide();
                    $(".fa-eye").show();
                    $("#password").prop("type", "text");
                } else {
                    $(".fa-eye-slash").show();
                    $(".fa-eye").hide();
                    $("#password").prop("type", "password");
                }
            }
            $("#cpassword_form").validate({
                errorClass: 'field-error',
                ignore: [],
                rules: {
                    current_password: {
                        required: true,
                         minlength: 5,
                    },
                    password: {
                        required: true,
                        minlength: 5,
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 5,
                        equalTo: "#password"
                    },
                },
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form, element) {
                    $(".submit_buttom").html('Please wait...').prop('disabled', true);
                    fm = new FormData(form);
                    $.ajax({
                        url: '{{ Url('profile/settings/change_password') }}',
                        type: 'post',
                        data: fm,
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            if (data.success == true) {
                                $('#cpassword_form').find('input:password').val('');
                                swal("Success!", data.message, "success");
                            } else {
                                swal(data.title, data.message, "error");
                            }
                        },
                        error: function(data) {
                            swal(data.responseJSON.title || 'Error !', data.responseJSON.message || data
                                .statusText, "error");
                        },
                        complete: function(data) {
                            $(".submit_buttom").html('Update').prop('disabled', false);
                        }
                    })
                }
            })
        </script>
    @endpush
