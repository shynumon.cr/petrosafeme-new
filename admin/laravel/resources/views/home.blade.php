@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<div class="row cm-content-section m-0">
    <div class="col-12 page-title-main pl-0 pr-0">
        <ul>
            <li>
                <h4 class="MyriadPro-Bold">Dashboard</h4>
            </li>
            <li class="float-right">Last Login: <span
                    class="text-blue bold">{{ date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime)) }}</span>
            </li>
        </ul>
    </div>
</div>
<div class="card-body">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @role('admin')
        {{ __('You are admin') }}
    @endrole
    @role('manager')
        {{ __('You are user') }}
    @endrole
<?php
/*echo "<pre>";
print_r(auth()->user()->permissions);
echo "<pre>";*/
    /*if(auth()->user()->hasPermissionTo('list-user')){
        echo "You can List user";
    }else{
        echo "can't List user";
    }*/
echo "<br/>";
    if(auth()->user()->can('list-user')){
        echo "You can List user";
    }else{
        echo "can't List user";
    }
    echo "<br/>";
    if(auth()->user()->can('all-permissions')){
        echo "You can all permission";
    }else{
        echo "can't all permission";
    }

    echo "<br/>";
    if(auth()->user()->can('create-user')){
        echo "You can Create user";
    }else{
        echo "can't Create user";
    }
echo "<br/>";
    if(auth()->user()->can('edit-user')){
        echo "You can Edit user";
    }else{
        echo "can't Edit user";
    }
    echo "<br/>";

    echo "<br/>";
    if(auth()->user()->can('delete-user')){
        echo "You can Edit user";
    }else{
        echo "Can't Edit user";
    }
    echo "<br/>";
?>
</div>
@endsection
@push('styles')
        <link rel="stylesheet" href="{{ URL::asset('css/animate.min.css') }}" />
    @endpush
    