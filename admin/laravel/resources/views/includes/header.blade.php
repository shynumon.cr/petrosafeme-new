
<header>
    <script>
        var _base_url = '{{ url('') }}/';
        </script>
    <div class="row m-0">
        <div class="col-md-12 top-menu p-0">
            <ul>
                <li class="logo">
                    <a href="#">
                        <img src="{{ URL::asset('images/logo2.png') }}" alt="" class="d-none d-lg-block">
                    </a>
                </li>
                <li class="menu-icon"><img src="{{ URL::asset('images/menu.png') }}" alt=""></li>
                <li class="login-admin top-dropdown-set">
                    <div class="row m-0">
                        <div class="col w-auto admin-pic p-0"><img src="{{ URL::asset('images/blue-user-icon.jpg') }}"
                                alt=""></div>
                    <div class="col w-auto admin-text">
                            <p>{{ ucfirst(Auth::user()->LoginName) }} <i class="fa fa-chevron-circle-down"></i></p>
                        </div>
                    </div>
                    <div class="top-dropdown">
                        <ul>
                            <li><a href="{{ url('profile/settings/change_password') }}"><i class="fa fa-user"></i> Change Password</a></li>
                            <li class="logout-btn"><a href="{{ url('logout') }}"><i class="fa fa-power-off"></i>
                                    Logout</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-sm-12 main-nav p-0">
            <nav id="primary_nav_wrap">
                <ul>
                    <li><a href="{{url('home')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li>
                        <a href="#"><i class="fa fa-ticket"></i> Masters</a>
                        <ul>
                            <li><a href="{{url('team-list')}}">Team</a></li>
                            <li><a href="{{url('area-list')}}">Area</a></li>
                            <!-- <li><a href="{{url('user-list')}}">Users</a></li> -->
                        </ul>
                    </li>
                    <li>
                        <a href="{{url('time-slot')}}">Time slot Management</a>
                    </li>
                    <li>
                        <a href="{{url('booking-list')}}">Bookings</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-users"></i> User Management</a>
                        <ul>
                            
                          
                            <li><a href="{{url('modules-list')}}">Modules</a></li>
 
                            <li><a href="{{url('permission-list')}}">Permissions</a></li>
<?php if(auth()->user()->can('list-role') || auth()->user()->can('all-permissions') ){
    ?>

                            <li><a href="{{url('roles-list')}}">Roles</a></li>

<?php }
if(auth()->user()->can('list-user') || auth()->user()->can('all-permissions')){
    ?>
                            <li><a href="{{url('user-list')}}">Users List</a></li>
<?php } ?>
                        </ul>
                    </li>
                </ul>
            </nav>

        </div>
    </div>
</header>
<style>
    #primary_nav_wrap li a.active,
    #primary_nav_wrap li:has(ul li a.active) {
        background-color: #7ec4e5;
    }
</style>
