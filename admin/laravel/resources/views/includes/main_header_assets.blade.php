<link rel="icon" type="image/png" href="{{URL::asset('images/fav.png')}}"/>
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/bootstrap.css')}}" >
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/font-awesome.min.css')}}"/>
<link rel="stylesheet"  href="{{URL::asset('css/choices.min.css')}}">
<link rel="stylesheet"  href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet"  href="{{URL::asset('css/datatables.min.css')}}">
<link rel="stylesheet"  href="{{URL::asset('css/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css?v='.cssVersion())}}"/>
@stack('styles')