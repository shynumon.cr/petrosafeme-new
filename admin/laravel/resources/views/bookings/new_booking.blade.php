@extends('layouts.main')
@section('content')
    {{-- <?php
    $per = getPermission();
    ?> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="add-member-popup-wrapper">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main col-sm-5 mx-auto">
                <div class="close-btn"><img src="{{ URL::asset('images/white-close.png') }}" alt=""></div>
                <h5 class="MyriadPro-Bold modal_title">Add Booking</h5>
                <div class="col-12 step-cont" id="cont1">

                </div>
            </div>
        </div>
    </div>

    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">Add Booking</h4>
                </li>
                {{-- <li class="float-right">Last Login: <span
                        class="text-blue bold">{{ date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime)) }}</span>
                </li> --}}
            </ul>
        </div>
        <!--page-title-main end-->

        <div class="col-12 cm-content-main p-3">
            <form id="booking_form" class="row" enctype="multipart/form-data">
                <div class="row m-0">
                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>Customer Name<span class="text-danger">*</span></p>
                        </b>
                        <input class="input-field" id="customer_name" name="customer_name" type="text">
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>Contact No<span class="text-danger">*</span></p>
                        </b>
                        <input class="input-field" id="mob_no" name="mob_no" type="number">
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>Address</p>
                        </b>
                        <textarea name="address" id="address" class="input-field"></textarea>
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>Flat/Apt No<span class="text-danger">*</span></p>
                        </b>
                        <input class="input-field" name="flat_no" id="flat_no" type="text">
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>Building Name<span class="text-danger">*</span></p>
                        </b>
                        <input class="input-field" id="building_name" name="building_name" type="text">
                    </div>

                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>WhatsApp NO</p>
                        </b>
                        <input class="input-field" id="whatsapp_no" name="whatsapp_no" type="number">
                    </div>

                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>Date of Service<span class="text-danger">*</span></p>
                        </b>
                        <input type="date" name="service_date" min="<?php echo date('Y-m-d'); ?>" type="text"
                            id="service_date">
                        <input type="hidden" name="id">
                    </div>

                    <div class="row-12 inline-block ml-3">
                        <b>
                            <p>Booking Type<span class="text-danger">*</span></p>
                        </b>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="booking_type" name="booking_type"
                                value="CON">
                            <label class="form-check-label" for="booking_type_new">New Connection</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="booking_type_dis" name="booking_type"
                                value="DIS">
                            <label class="form-check-label" for="booking_type_dis">Disconnection</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="booking_type_shift" name="booking_type"
                                value="SHIF">
                            <label class="form-check-label" for="booking_type_shift">Shifting</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="booking_type_recon" name="booking_type"
                                value="RECON">
                            <label class="form-check-label" for="booking_type_recon">Reconnection</label>
                        </div>
                    </div>

                    <div class="col-sm-6 cm-field-main cm-field" id="new_building_div">
                        <b>
                            <p>New Building Name<span class="text-danger">*</span></p>
                        </b>
                        <input class="input-field" id="new_building_name" name="new_building_name" type="text">
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="new_flat__div">
                        <b>
                            <p>New Flat/Apt No<span class="text-danger">*</span></p>
                        </b>
                        <input class="input-field" name="new_flat_no" id="new_flat_no" type="text">
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="shifting_to_div">
                        <b>
                            <p>Shifting Required To<span class="text-danger">*</span></p>
                        </b>
                        <input class="input-field" name="shifting_to" id="shifting_to" type="text">
                    </div>

                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>Team<span class="text-danger">*</span></p>
                        </b>
                        <select name="team" id="team" class="input-field">
                            <option value="">-- Select Team --</option>
                            @foreach ($team as $t)
                                <option value="{{ $t->team_id }}">{{ $t->team_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>Area<span class="text-danger">*</span></p>
                        </b>
                        <select name="area" id="area" class="input-field">
                            <option value="">-- Select Area --</option>

                        </select>
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>Timings<span class="text-danger">*</span></p>
                        </b>
                        <select name="slot_id" id="slot_id" class="input-field">
                            <option value="">-- Select Time Slot --</option>
                            @foreach ($time_slots as $slot)
                                <option value="{{ $slot->slot_id }}">
                                    {{ date('h:i A', strtotime($slot->from_slot)) }} -
                                    {{ date('h:i A', strtotime($slot->to_slot)) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="gas_available_div">
                        <b>
                            <p>Cooking Range/Burner/Gas stove available</p>
                        </b>
                        <select name="gas_available" id="gas_available" class="input-field">
                            <option value="">-- Select --</option>
                            <option value="YES">Yes</option>
                            <option value="NO">No</option>
                        </select>
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="availabilty_div">
                        <b>
                            <p>Will You Available in Apartment during Disconnection</p>
                        </b>
                        <select name="availability" id="availability" class="input-field">
                            <option value="">-- Select --</option>
                            <option value="YES">Yes</option>
                            <option value="NO">No</option>
                        </select>
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="dewa_conect_div">
                        <b>
                            <p>Active DEWA Connection</p>
                        </b>
                        <select name="active_dewa_connection" id="active_dewa_connection" class="input-field">
                            <option value="">-- Select --</option>
                            <option value="YES">Yes</option>
                            <option value="NO">No</option>
                        </select>
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="pay_mode_div">
                        <b>
                            <p>Payment Mode<span class="text-danger">*</span></p>
                        </b>
                        <select name="payment_mode" id="payment_mode" class="input-field">
                            <option value="">-- Select Payment Mode --</option>
                            <option value="CASH">Cash</option>
                            <option value="CREDIT_CARD">Credit Card</option>
                            <option value="ONLINE">Online</option>
                        </select>
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field">
                        <b>
                            <p>Comments</p>
                        </b>
                        <textarea name="comments" id="comments" class="input-field"></textarea>
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="eid_front_div">
                        <b>
                            <p>EID Front</p>
                        </b>
                        <input type="file" name="eid_front" id="eid_front" class="input-field">
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="eid_back_div">
                        <b>
                            <p>EID Back</p>
                        </b>
                        <input type="file" name="eid_back" id="eid_back" class="input-field">
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="tenacy_div">
                        <b>
                            <p>Tenancy</p>
                        </b>
                        <input type="file" name="tenancy" id="tenancy" class="input-field">
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="meter_div">
                        <b>
                            <p>Meter Picture</p>
                        </b>
                        <input type="file" name="meter_picture" id="meter_picture" class="input-field">
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field" id="gas_cooker_div">
                        <b>
                            <p>Gas Cooker Photo</p>
                        </b>
                        <input type="file" name="gas_cooker_photo" id="gas_cooker_photo" class="input-field">
                    </div>
                    <div class="col-sm-6 cm-field-main cm-field ml-2" id="gas_contract_div">
                        <b>
                            <p>Gas Contract/Deposit Voucher/Receipt<span class="text-danger">*</span></p>
                        </b>
                        <select name="gas_contract_type" id="gas_contract_type" class="input-field">
                            <option value="">-- Select Document Type --</option>
                            <option value="Gas Contract">Gas Contract</option>
                            <option value="Deposit Voucher">Deposit Voucher</option>
                            <option value="Receipt">Receipt</option>
                        </select>
                        <div class="col-sm-6 cm-field-main cm-field p-3">
                            <b>
                                <p>Upload</p>
                            </b>
                            <input type="file" name="gas_contract_image" id="gas_contract_image" class="input-field">
                        </div>
                    </div>

                    <div class="col-sm-12 cm-field-btn">
                        <button type="submit" id="sbmt_btn" class="submit_button field-btn CM font-weight-normal w-auto">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--cm-content-section end-->
@endsection
@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/booking.css') }}">
    <script type="text/javascript" src="{{ URL::asset('public/js/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/js/booking.js') }}"></script>



    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // ************************************************
        $(document).ready(function() {
            $('#team').change(function() {
                var teamId = $(this).val();
                if (teamId) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('get-area-by-team') }}",
                        data: {
                            'id': teamId
                        },
                        success: function(res) {
                            if (res.status == 'success') {
                                $('#area').empty();
                                $('#area').append(
                                    '<option value="">-- Select Area --</option>');
                                $.each(res.data, function(key, value) {
                                    $('#area').append('<option value="' + value.id +
                                        '">' + value.area_name + '</option>');
                                });
                            } else {
                                $('#area').empty();
                                $('#area').append(
                                    '<option value="">-- No Areas Found --</option>');
                            }
                        }
                    });
                } else {
                    $('#area').empty();
                    $('#area').append('<option value="">-- Select Area --</option>');
                }
            });
        });
    </script>

    <style>
        .error {
            color: red;
            font-size: 13px;
            display: block;
            text-align: left;
        }
    </style>
@endpush
