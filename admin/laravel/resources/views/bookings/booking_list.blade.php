@extends('layouts.main')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">



    <div class="common-popup-wrapper" id="delete_popup" style="display:none;">
        <div class="add-member-popup-section d-flex">
            <div class="add-member-popup-main mx-auto col-lg-4 col-md-6 col-sm-6">
                <div class="close-btn"><img src="{{ URL::asset('images/white-close.png') }}" alt=""></div>
                <div class="common-popup-icon m-auto"><img src="{{ URL::asset('images/alert.gif') }}" alt=""></div>
                <h5 class="MyriadPro-Bold text-center">Are you sure to delete</h5>
                <div class="common-popup-text">
                    <div class="col-12 cm-field-main cm-field p-0 pr-0 text-center mt-4">
                        <input type="button" class="field-btn CM font-weight-normal w-auto Danger" value="Delete">
                        <input type="button" class="field-btn font-weight-normal w-auto" value="Cancel">
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Pop Up -->

    <div class="row cm-content-section m-0">
        <div class="col-12 page-title-main">
            <ul>
                <li>
                    <h4 class="MyriadPro-Bold">Booking List</h4>
                </li>
                {{-- <li class="float-right">Last Login: <span
                        class="text-blue bold">{{ date('F d, Y h:i a', strtotime(Auth()->user()->lastLoginTime)) }}</span>
                </li> --}}
            </ul>
        </div>
        <!--page-title-main end-->

        <div class="col-12 cm-content-main">
            <div class="col-12 table-main">
                <div class="col-12 table-filter">
                    <ul class="filter_list">
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Search Keyword</p>
                                <input class="input-field" placeholder="Search Here" id="search" type="text">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Start Date</p>
                                <input class="form-control input-field calendar" placeholder="Start Date" id="start_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>End Date</p>
                                <input class="form-control input-field calendar" placeholder="End Date" id="end_date"
                                    type="text" data-select="datepicker">
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="search()"><i class="fa fa-search"
                                        aria-hidden="true"></i>Search</span>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Sort by</p>
                                <select name="" id="sub_status" class="time-slot" onchange="sortData()";>
                                    <option value="">Select from list</option>
                                    <option value="name.asc">Name A-Z</option>
                                    <option value="name.desc">Name Z-A</option>
                                    <option value="created_at.asc">Created ></option>
                                    <option value="created_at.desc">Created < </option>
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="cm-field-main m-0">
                                <p>Show By</p>
                                <select name="show_by" id="show_by">
                                    @foreach ([10, 25, 50, 100] as $it)
                                        <option {{ $it == 25 ? 'selected' : '' }} value="{{ $it }}">
                                            {{ $it }}
                                            entries</option>
                                    @endforeach
                                </select>
                            </div>
                        </li>
                        <li class="float-end">
                            <div class="cm-field-main m-0">
                                <p class="d-none d-md-block">&nbsp;</p>
                                <span class="CM filter-btn" onclick="new_booking();"><i class="fa fa-plus"
                                        aria-hidden="true"></i> New Booking</span>
                            </div>
                        </li>
                    </ul>
                </div>

                <table id="company_table" class="table table-sm table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>
                                Sl. No
                            </th>
                            <th>
                                Title
                            </th>
                            <th>
                                Arabic Title
                            </th>
                            <th>
                                Ticket Message
                            </th>
                            <th>
                                Arabic Ticket Message
                            </th>
                            <th>
                               CRM ID
                            </th>
                            <th>
                                Status
                            </th>
                            <th class="action">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!--cm-content-main end-->
        </div>
    </div>
    <!--cm-content-section end-->
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var studentTable = '';
        $(document).ready(function() {
            $("#start_date,#end_date").datepicker({
                autoclose: true
            });
            studentTable = $('#company_table').DataTable({
                'bFilter': false,
                'bLengthChange': false,
                'pageLength': 25,
                'processing': true,
                'serverSide': true,
                'bSort': false,
                'serverMethod': 'post',
                'ajax': {
                    'url': '{{ url('ticket_list') }}',
                    'data': function(data) {
                        data.from_date = dmy($('#start_date').val());
                        data.to_date = dmy($('#end_date').val());
                        data.keywordsearch = $('#search').val();
                        data.sub_status = $('#sub_status').val();
                        data.filter_status = $('#filter_status').val();
                    },
                    "complete": function(json, type) {
                        var _resp = $.parseJSON(json.responseText);
                    }
                },
                'columns': [{
                        data: 'slno'
                    },
                    {
                        data: 'title'
                    },
                    {
                        data: 'arabicTitle'
                    },
                    {
                        data: 'ticketMessage'
                    },
                    {
                        data: 'arabicTicketMessage'
                    },
                    {
                        data: 'crm_id'
                    },
                    {
                        data: 'ticketstatus',
                        render: function(data, type, row) {
                            var statustext = '';
                            var bgcolor = '';
                            var borderradius = '5px';

                            switch (data) {
                                case 0:
                                    statustext = 'Pending';
                                    bgcolor = '#dc3545';
                                    break;
                                case 1:
                                    statustext = 'Active';
                                    bgcolor = 'green';
                                    break;
                                case 2:
                                    statustext = 'Closed';
                                    bgcolor = 'primary';
                                    break;
                                case 3:
                                    statustext = 'Cancelled';
                                    bgcolor = 'warning';
                                    break;
                                default:
                                    statustext = 'Unknown Status';
                                    break;
                            }

                            return '<span class="btn btn-sm text-light bg-' + bgcolor +
                                '" style="background-color: ' + bgcolor + '; border-radius: ' +
                                borderradius + ';">' + statustext + '</span>';
                        }

                    },
                    {
                        data: 'action'
                    },
                ]
            });

            $("#booking_form").validate({
                ignore: [],
                rules: {
                    title: {
                        required: true
                    },
                    arabicTitle: {
                        required: true
                    },
                    ticketMessage: {
                        required: true
                    },
                    arabicTicketMessage: {
                        required: true
                    },
                    category_name: {
                        required: true
                    },
                    priority_name: {
                        required: true
                    },
                    customer_name: {
                        required: true
                    },
                },
                messages: {
                    title: "Enter a Title",
                    arabicTitle: "Enter a Arabic Title",
                    ticketMessage: "Enter a Ticket Message",
                    arabicTicketMessage: "Enter a Arabic Ticket Message",
                    category_name: "Select a Category",
                    priority_name: "Select a Priority",
                    customer_name: "Select a Customer",
                },

                errorPlacement: function(err, ele) {
                    err.insertAfter(ele);
                },
                submitHandler: function(form, element) {
                    $(".submit_buttom").html('<i class="fa fa-spinner fa-spin"></i> Please Wait').prop(
                        'disabled', true);
                    fm = new FormData(form);
                    $.ajax({
                        url: '{{ Url('ticket-new-add') }}',
                        type: 'post',
                        data: fm,
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            if (data.status == 'success') {
                                $("#booking_form")[0].reset();
                                $(".add-member-popup-wrapper").hide(500);
                                studentTable.draw();
                                swal("Success!", data.message, "success");
                                $(".submit_buttom").html('Save').prop('disabled', false);
                            } else {
                                $(".add-member-popup-wrapper").hide(500);
                                $(".submit_buttom").html('Save').prop('disabled', false);
                                swal("Error!", data.message, "error");
                            }
                        },
                        error: function(data) {
                            $(".add-member-popup-wrapper").hide(500);
                            $(".submit_buttom").html('Save').prop('disabled', false);
                            swal("Error!", data.statusText, "error");
                        }
                    })
                }
            })

            $('#show_by').change(function() {
                studentTable.page.len($(this).val()).draw();
            })
        });

        function new_booking() {
            window.location.href='{{url('new-booking')}}';
        }

        function delete_ticket(id) {
            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this Ticket !",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '{{ Url('delete-ticket-new') }}',
                            type: 'post',
                            data: {
                                id: id
                            },
                            success: function(data) {
                                if (data.status == 'success') {
                                    studentTable.draw();
                                    swal("Success!", data.message, "success");
                                } else {
                                    swal("Error!", data.message, "error");
                                }
                            },
                            error: function(data) {
                                swal("Error!", data.statusText, "error");
                            }

                        })
                    }
                });
        }

        function view_ticket(id) {
            location.href = '{{ Url('ticket-details') }}/' + id;
        }

        function search() {
            studentTable.draw();
        }

        function sortData() {
            studentTable.draw();
        }

        function edit_ticket(id) {
            $.ajax({
                url: '{{ Url('get-ticket-new') }}',
                type: 'post',
                data: {
                    id: id
                },
                success: function(data) {
                    if (data.status == 'success') {
                        put_fields(data.data);
                    } else {
                        swal("Error!", data.message, "error");
                    }
                }

            })
        }

        function put_fields(a) {
            $("[name=title]").val(a.title);
            $("[name=arabicTitle]").val(a.arabicTitle);
            $("[name=ticketMessage]").val(a.ticketMessage);
            $("[name=arabicTicketMessage]").val(a.arabicTicketMessage);
            $("[name=category_name]").val(a.ticketCategoryId).prop('selected', true);
            $("[name=priority_name]").val(a.priorityId).prop('selected', true);
            $("[name=customer_name]").val(a.customerId).prop('selected', true);
            $("[name=id]").val(a.id);
            $('.modal_title').html('Edit Ticket');
            $(".submit_buttom").html('Update');
            $(".add-member-popup-wrapper").show(500);
        }

        function dmy(a) {
            if (a == '')
                return '';
            a = a.split('/');
            b = [];
            b.push(a[2]);
            b.push(a[0]);
            b.push(a[1]);
            b = b.join('-');
            return b;
        }

        // *******************************************************************
        function push_to_crm(id) {
                swal({
                    title: "Are you sure?",
                    text: "Want to Push To CRM!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((push) => {
                    if (push) {
                        $.ajax({
                            url: '{{ Url('check-crm-exist-ticket') }}',
                            type: 'post',
                            data: {
                                id: id
                            },
                            success: function(data) {
                                if (data.status == 'success') {
                                    studentTable.draw();
                                    swal("Success!", data.message, "success");
                                } else {
                                    swal("Error!", data.message, "error");
                                }
                            },
                            error: function(data) {
                                swal("Error!", data.statusText, "error");
                            }
                        });
                    }
                });
            }
// **************************************************************
    </script>
    <style>
        .error {
            color: red;
            font-size: 13px;
            display: block;
            text-align: left;
        }

        .filter_list li {

        }
    </style>
@endpush
