@extends('theme.default')
@section('pagestyle')
<link rel="stylesheet" href="{{ asset('theme/assets/vendors/data-tables/datatables-bootstrap/dataTables.bootstrap4.css') }}">

@endsection
@section('content')
<div class="container-fluid">
	<div class="title-box">
		<div class="row">
			<div class="col-12 col-sm-6 col-xl-6"><div class="page-title">User Roles Management System</div>

		</div>
		<div class="col-12 col-sm-6 col-xl-6 "><div class="breadcrumb">User Part <span><i class="fas fa-angle-right"></i></span>User Role List</div></div>
	</div>
	<div class="row"><div class="col-12 col-sm-12 col-xl-12" style="text-align: right;"><a href='{{url("/add-role/")}}' class="theme btn btn-primary ">Link User-Role</a></div></div>
</div>

<div class="content-box">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body p-0">
					@if(count($all_roles)>0) 
					<div class="table-responsive">
						<table id="dataexample" class="table">
							<thead>
								<tr>
									<th>Role Name</th>
									<th>Slug</th>

								</tr>
							</thead>
							<tbody>
								@foreach($all_roles as $role)
								<tr>
									<td>{{$role->name}}</td>
									<td>{{$role->slug}}</td>
									<td><!-- <a class="btn btn-success mb-3 mr-2" href='{{url("/roles-show/{$role->id}")}}'>View</a> --><a class="btn btn-primary mb-3 mr-2" href='{{url("/roles-edit/{$role->id}")}}'>Edit</a><a class="btn btn-danger mb-3 mr-2" href='{{url("/roles-destroy/{$role->id}")}}'>delete</a></td>
								</tr>
								@endforeach

							</tbody>
						</table>
						@else
						<p>No Role Available</p>
						@endif
					</div>
				</div>
			</div>
		</div>           
	</div>
</div>

</div>
@endsection

@section('pagescript')

<!--Data tables JS -->  
<script src="{{ asset('theme/assets/js/slimscroll.min.js') }}"></script>
<script src="{{ asset('theme/assets/vendors/data-tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('theme/assets/vendors/data-tables/datatables-bootstrap/dataTables.bootstrap4.js') }}"></script>
<!-- Theme main JS -->  
<script src="{{ asset('theme/assets/js/main.js') }}"></script>
<script src="{{ asset('theme/assets/js/data-table.js') }}"></script>  


@endsection