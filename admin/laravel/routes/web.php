<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\TimeSlotController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('auth/login');
});

/*Route::post('/login', [
    'uses'          => 'Auth\LoginController@login',
    'middleware'    => 'CheckUserSite',
]);*/
/*Route::get('/', function () {
    return view('dashboard');
});*/
/*Route::get('/', function () {
    if (Auth::check())
        return redirect('dashboard');
    return view('auth/login');
})->withoutMiddleware('authcheck');
*/


/*Route::middleware(['auth','role:superadmin|admin'])->group(function () {*/
Route::group(['middleware' => 'role:superadmin'], function() {

/*Route::prefix('app')->name('app.')->middleware('authCheck:all-permission|organisation-values-list|organisation-values-create|organisation-values-edit|organisation-values-delete|organisation-vision-create|organisation-vision-view|organisation-vision-edit|organisation-mission-create|organisation-mission-edit|organisation-mission-view|organisation-purpose|goals-view,superadmin|admin|head|staff|associate|worker|lead|doer|consultant|client|developer')->group(function () {*/

//User
Route::get('user-list',[UserController::class,'user_view']);
Route::post('list-users', [UserController::class, 'list_users']);
Route::post('add-edit-user', [UserController::class, 'add_edit_user']);
Route::post('delete-user', [UserController::class, 'delete_user']);
Route::post('get-users', [UserController::class, 'get_user']);

//Roles
Route::get('roles-list',[RoleController::class,'index']);
Route::post('list-roles', [RoleController::class, 'list_roles']);
Route::post('add-edit-role', [RoleController::class, 'add_edit_role']);
Route::post('delete-role', [RoleController::class, 'delete_role']);
Route::post('get-roles', [RoleController::class, 'get_role']);

//Modules
Route::get('modules-list',[CategoryController::class,'index']);
Route::post('list-modules', [CategoryController::class, 'list_modules']);
Route::post('add-edit-module', [CategoryController::class, 'add_edit_module']);
Route::post('delete-module', [CategoryController::class, 'delete_module']);
Route::post('get-module', [CategoryController::class, 'get_module']);

//Permission
Route::get('permission-list',[PermissionController::class,'index']);
Route::post('list-permissions', [PermissionController::class, 'list_permissions']);
Route::post('add-edit-permission', [PermissionController::class, 'add_edit_permission']);
Route::post('delete-permission', [PermissionController::class, 'delete_permission']);
Route::post('get-permissions', [PermissionController::class, 'get_permission']);/**/



});

Route::group(['middleware' => ['auth']], function() {
    Route::get('logout', [UserController::class, 'logout']);
    Route::get('dashboard', [UserController::class, 'dashboard']);

//TEAM
Route::get('team-list',[TeamController::class,'team_view']);
Route::post('list-team', [TeamController::class, 'list_team']);
Route::post('add-edit-team', [TeamController::class, 'add_edit_team']);
Route::post('delete-team', [TeamController::class, 'delete_team']);
Route::post('get-team', [TeamController::class, 'get_team']);
//AREA
Route::get('area-list',[AreaController::class,'area_view']);
Route::post('list-area', [AreaController::class, 'list_area']);
Route::post('add-edit-area', [AreaController::class, 'add_edit_area']);
Route::post('delete-area', [AreaController::class, 'delete_area']);
Route::post('get-area', [AreaController::class, 'get_area']);


//Time slot Management
Route::get('time-slot',[TimeSlotController::class,'time_slot']);
Route::get('get-booking-count', [TimeSlotController::class,'get_booking_count_time_slot']);
Route::post('team-slot-add', [TimeSlotController::class,'team_slot_add_edit']);

// BOOKING
Route::get('booking-list',[BookingController::class,'booking_list']);
Route::get('new-booking', [BookingController::class,'new_booking']);
Route::post('add-edit-booking', [BookingController::class,'add_edit_booking']);
Route::get('get-area-by-team', [BookingController::class,'get_area_by_team']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});


Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    if($exitCode == 0){
        return "Cache cleared successfully !";
    }
});

Auth::routes();


