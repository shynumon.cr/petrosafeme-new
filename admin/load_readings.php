<?php  
		session_start();
	    $adminid=$_SESSION['adminid'];	  
	    $odoooid=$_SESSION['odooid'];
	    include('../utility/connection.php');
		
		
		$readingsQuery = "SELECT * FROM meter_reading_updates ORDER BY upd_added_datetime DESC";
	
	    $resultReadings = mysqli_query($con,$readingsQuery);
		
		$i=1;
		
		
		
		
		
		
		
				## Read value
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		
		$from_date=isset($_POST['Fromdate'])?$_POST['Fromdate']:'';
		$to_date=isset($_POST['Todate'])?$_POST['Todate']:'';
		$fromdate = str_replace('/', '-', $from_date);
		$frmdate = date('Y-m-d', strtotime($fromdate));
		$enddate = str_replace('/', '-', $to_date);
		$todate = date('Y-m-d', strtotime($enddate));
		
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = $_POST['search']['value']; // Search value

		## Search 
		$searchQuery2 = " ";
		if($searchValue != ''){
		   
				
			$searchQuery2 = " and (upd_cust_id like '%".$searchValue."%' or 
				upd_cust_name like '%".$searchValue."%' or 
				upd_cust_email like '%".$searchValue."%' or 
				upd_cust_building_name like'%".$searchValue."%' or 
				upd_cust_flat_name like'%".$searchValue."%' or 
				upd_cust_mob like'%".$searchValue."%' or 
				meter_number like'%".$searchValue."%' or 
				upd_reading like'%".$searchValue."%' ) ";
		}
		
		$date_qry="";
		
		if($frmdate!='1970-01-01'&&$todate=='1970-01-01')
	    {
		   //console.log('start date only');
		   
		   $date_qry=" and DATE(upd_added_datetime)>='".$frmdate."' ";
	    }
	    else if($frmdate=='1970-01-01'&&$todate!='1970-01-01')
	    {
		   
		   $date_qry=" and DATE(upd_added_datetime)<='".$todate."' ";
		   
	    }
	    else if($frmdate!='1970-01-01'&&$todate!='1970-01-01')
	    {
		    if(strtotime($frmdate) <= strtotime($todate)){
			   $date_qry=" and DATE(upd_added_datetime)>='".$frmdate."' and DATE(upd_added_datetime)<='".$todate."' ";
			}
			
	    }

		## Total number of records without filtering
		$sel = mysqli_query($con,"select count(*) as allcount from meter_reading_updates");
		$records = mysqli_fetch_assoc($sel);
		$totalRecords = $records['allcount'];

		## Total number of record with filtering
		$sel = mysqli_query($con,"select count(*) as allcount from meter_reading_updates WHERE 1 ".$searchQuery2.$date_qry);
		
		$records = mysqli_fetch_assoc($sel);
		$totalRecordwithFilter = $records['allcount'];

		## Fetch records
		$empQuery = "select * from meter_reading_updates WHERE 1 ".$searchQuery2.$date_qry." ORDER BY upd_added_datetime DESC limit ".$row.",".$rowperpage;
		$empRecords = mysqli_query($con, $empQuery);
		$data = array();
		$i=($row)+1;
		while ($row = mysqli_fetch_assoc($empRecords)) {
			$image='';
		   if(strlen($row['upd_image'])>3)
		   {
			   $imgfulllink=str_replace("thumbs/","",$row['upd_image']);
			   $imgthumblink=$row['upd_image'];
			   $image='<a href="https://petrosafeme.com/'.$imgfulllink.'" data-fancybox><img src="https://petrosafeme.com/'.$imgthumblink.'" style="width:110px;height:auto;"/></a>';
		   }
		   $added_datetime=$row['upd_added_datetime'];
		   $added_date=date('d/m/Y',strtotime($added_datetime));
		   $added_time=date('g:i A',strtotime($added_datetime));
		   $datetime_text=$added_date."<br/>".$added_time;
		   $reading_text='';$action_text='';$name_text='';
		   $invoice_date = date('Y-m-d',strtotime($added_datetime));
		   $readid=$row['upd_id'];
		   $reading=$row['upd_reading'];
		   $meterid=$row['meter_number'];
		   $custid=$row['upd_cust_id'];
		   if($row['upd_inv_create_stat']==0)
		    {
				$reading_text='<input type="number" value="'.$reading.'" class="read_edit readedit'.$readid.'" step="0.01" min="0.01"/>';
			    $action_text='<button type="button" class="btn mb-1 btn-primary btn-sm btn-dark create_inv invbtn'.$readid.'" data-readid="'.$readid.'" data-reading="'.$reading.'" data-custid="'.$custid.'" data-invdate="'.$invoice_date.'" data-odooid="'.$odoooid.'">Create Invoice</button>';
			}
		   else
			{
				$reading_text=$reading;
				$action_text='<button type="button" class="btn mb-1 btn-primary btn-sm btn-success invoiced" disabled>Invoiced</button>';
				
			}
		    if($row['upd_cust_flat_name']){$name_text.=",<br/>Flat - ".$row['upd_cust_flat_name']."";}
			if($row['upd_cust_mob']){$name_text.=",<br/>Mob - ".$row['upd_cust_mob']."";}
			if($row['upd_cust_email']){$name_text.=",<br/>Email - ".$row['upd_cust_email']."";}
		   
		   $data[] = array( 
			  "sl_no"=>$i,
			  "upd_cust_id"=>$row['upd_cust_id'],
			  "upd_cust_name"=>$row['upd_cust_name'].$name_text,
			  "upd_cust_building_name"=>$row['upd_cust_building_name'],
			  "upd_added_datetime"=>$datetime_text,
			  "meter_number"=>$meterid,
			  "last_reading"=>$row['previous_reading_date'],
			  "upd_image"=>$image,
			  "upd_reading"=>$reading_text,
			  "action"=>$action_text
			  
		   );
		   $i++;
		}

		## Response
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $totalRecords,
		  "iTotalDisplayRecords" => $totalRecordwithFilter,
		  "aaData" => $data,
		  "sql" => $empQuery
		);

		echo json_encode($response);
	   
?>