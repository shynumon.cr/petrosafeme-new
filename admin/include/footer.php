		<div class="footer">
            <div class="copyright">
                <div class="text-center"> Copyrights &amp; Powered by <a href="http://azinovatechnologies.com/" target="_blank"><span class="azinova-logo"></span><!--azinova-logo end--></a></div>
            </div>
        </div>
	</div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

<!--**********************************
        Scripts
    ***********************************-->
    <script src="js/custom.min.js"></script>
	<script src="js/settings.js"></script>
	<script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
	<script src="js/jquery.fancybox.js"></script>
	<script src="js/bootstrap-tagsinput.js"></script>
	 
	
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!--<script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>-->
	<script src="./js/dashboard/dashboard-1.js"></script>
    <!-- Chartjs -->
    <script src="js/jquery.dataTables.min.js"></script>
    
    <script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>
	<script src="./plugins/summernote/dist/summernote.min.js"></script>
	<script src="./plugins/sweetalert/js/sweetalert.min.js"></script>
	<!--<script src="js/dataTables.buttons.min.js"></script>
	<script src="js/jszip.min.js"></script>
	<script src="js/pdfmake.min.js"></script>
	<script src="js/vfs_fonts.js"></script>
	<script src="js/buttons.html5.min.js"></script>-->
    
	<script>
	var editor;
	$(document).ready(function(){
	  if ($('.select2').length) 
	  {
		  $('.select2').select2();
	  }
	  
	  if ($('.summernote').length) 
	  {
		  $('.summernote').summernote({height:450,minHeight:null,maxHeight:null,focus:!1});
	  }
		
	  if ($('#cust_table').length) 
	  {	
		   var custtable=$('#cust_table').DataTable({
			  'processing': true,
			  'serverSide': true,
			  'bSort' : false,
			  'serverMethod': 'post',
			  'ajax': {
				  'url':'load_payments.php',
				  'data': function(data){
					if($('.fromdate').length)
						{
							var from_date = $('.fromdate').val();
						    var to_date = $('.todate').val();
						  
						    data.Fromdate = from_date;
						    data.Todate = to_date;
						}
					else
						{
							var today=new Date();
							var today_date=((today.getDate()<10) ? '0'+today.getDate() : today.getDate())+'/'+((today.getMonth()<9) ? '0'+(today.getMonth()+1) : (today.getMonth()+1))+'/'+today.getFullYear();
							console.log(today_date);
							data.Fromdate = today_date;
						    data.Todate = today_date;
						}
					  
				   }				  
			  },
			  'columns': [
				 { data: 'sl_no' },
				 { data: 'cust_name' },
				 { data: 'amount' },
				 { data: 'building' },
				 { data: 'flat' },
				 { data: 'order' },
				 { data: 'reference' },
				 { data: 'status' },
				 { data: 'init_time' }
			  ],
			  /*dom: 'Bfrtip',
			  buttons: ['excelHtml5'],
			  exportOptions: {modifer: {page: 'all'}}*/
		   });
		   
		   var datefilter_html='<div class="pull-left">Start Date&nbsp;<input value="" class="dt_tbl_ip fromdate"/>&nbsp;&nbsp;End Date&nbsp;<input value="" class="dt_tbl_ip todate"/></div>';
		   $(datefilter_html).appendTo("#cust_table_wrapper .dataTables_filter"); //example is our table id
		   $( ".fromdate,.todate" ).datepicker({dateFormat: 'dd/mm/yy'}).datepicker("setDate", new Date());
		   
		   $(".fromdate,.todate").change(function()
		   {			   
			   var start_date=$(".fromdate").val();
			   var to_date=$(".todate").val();
			   
			   if(start_date!=''&&to_date=='')
			   {
				   //console.log('start date only');
				   custtable.ajax.reload(null, false);
			   }
			   else if(start_date==''&&to_date!='')
			   {
				   //console.log('to date only');
				   custtable.ajax.reload(null, false);
			   }
			   else
			   {
				   if(process(to_date) >= process(start_date)){
					   custtable.ajax.reload(null, false);
					}
				   else
				   {
					   if(start_date!=''&&to_date!='')
					   {
						   swal('End date cannot be lesser than Start date');
						   $(".todate").val('');
					   }
				   }
			
			   }
			   
			   
			});
	  }
	  
	  
	  if ($('#read_table').length) 
	  {	
		   var myTable=$('#read_table').DataTable({
			  'processing': true,
			  'serverSide': true,
			  'bSort' : false,
			  'serverMethod': 'post',
			  'ajax': {
				  'url':'https://petrosafeme.com/admin/load_readings.php',
				  'data': function(data){					  
					  if($('.fromdate').length)
						{
							var from_date = $('.fromdate').val();
						    var to_date = $('.todate').val();
						  
						    data.Fromdate = from_date;
						    data.Todate = to_date;
						}
					else
						{
							var today=new Date();
							var today_date=((today.getDate()<10) ? '0'+today.getDate() : today.getDate())+'/'+((today.getMonth()<9) ? '0'+(today.getMonth()+1) : (today.getMonth()+1))+'/'+today.getFullYear();
							data.Fromdate = today_date;
						    data.Todate = today_date;
						}
				   }
			  },
			  'columns': [
				 { data: 'sl_no' },
				 { data: 'upd_cust_id' },
				 { data: 'upd_cust_name' },
				 { data: 'upd_cust_building_name' },
				 { data: 'upd_added_datetime'},
				 { data: 'meter_number'},
				 { data: 'last_reading'},
				 { data: 'upd_image' },
				 { data: 'upd_reading' },
				 { data: 'action'}
			  ]
		   });
		   
		   var select_html='<?php echo $select_html;?>';
		   
		   $('#read_table').on( 'click', 'tbody td .create_inv', function () {
			   var read_id=$(this).data("readid");
			   var reading=$(".readedit"+read_id).val();
			   var cust_id=$(this).data("custid");
			   var invoicedate=$(this).data("invdate");
			   var user_id=$(this).data("odooid");
			   var params = new Object();
			   params.customer_id = cust_id;
			   params.meter_reading  = parseFloat(reading).toFixed(3);
			   params.user_id = user_id;
			   var postarray=new Object();
			   postarray.params = params;
			   var jsonString= JSON.stringify(postarray);
			   //console.log('reading='+reading+'parsed_reading ='+parseFloat(reading));
		   
			      $(".invbtn"+read_id).html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>');
				  $(".invbtn"+read_id).attr("disabled", true);
				  $.ajax({
						url:"create_invoice.php",
						method:"POST",
						data:{'customer_id':cust_id,'meter_reading':parseFloat(reading),'user_id':user_id,'update_id':read_id,'invoicedates':invoicedate},
						dataType: 'json',
						success:function(response) {
							var reading_id_stat=response.result.status;
							
							if(reading_id_stat=='success')
							{
								$(".invbtn"+read_id).html('Invoiced');	
								$(".invbtn"+read_id).removeClass("create_inv");								
								$(".invbtn"+read_id).addClass("invoiced");
								$(".readedit"+read_id).parent().html(reading);								
								
							}
							else
							{
								$(".invbtn"+read_id).html('Create Invoice');
								$(".invbtn"+read_id).attr("disabled", false);
								alert('Invoice creation error!Try again later.');
							}
								
					   },
					   error:function(response){
						    $(".invbtn"+read_id).html('Create Invoice');
							$(".invbtn"+read_id).attr("disabled", false);
							alert('Invoice creation error!Try again later.');
					   }

					  });
				 
			   
				
			} );
			
		   $(select_html).appendTo("#read_table_wrapper .dataTables_filter"); //example is our table id
			
			$(".dataTables_filter label").addClass("pull-right");
			$( ".fromdate,.todate" ).datepicker({dateFormat: 'dd/mm/yy'}).datepicker("setDate", new Date());
			
			$(".fromdate,.todate").change(function()
		    {			   
			   var start_date=$(".fromdate").val();
			   var to_date=$(".todate").val();
			   
			   if(start_date!=''&&to_date=='')
			   {
				   //console.log('start date only');
				   myTable.ajax.reload(null, false);
			   }
			   else if(start_date==''&&to_date!='')
			   {
				   //console.log('to date only');
				   myTable.ajax.reload(null, false);
			   }
			   else
			   {
				   if(process(to_date) >= process(start_date)){
					   myTable.ajax.reload(null, false);
					}
				   else
				   {
					   if(start_date!=''&&to_date!='')
					   {
						   swal('End date cannot be lesser than Start date');
						   $(".todate").val('');
					   }
				   }
			
			   }
			   
			   
			});


			$('.table-responsive').on( 'change', '#build_search', function () {
				var building=$(this).val();
				$("#read_table_filter input[type=search]").val(building);
				$("#read_table_filter input[type=search]").keyup();
				$("#read_table_filter input[type=search]").val('');
			});
			
			$("#read_table_filter input[type=search]").keydown(function(){
			  $("#build_search").val("");
			});

	  }

	  if ($('#exp_pay_btn').length) 
	  {
		$('#exp_pay_btn').click(function()
		{
			
			var baseurl=$('#base_url').val();
		    var fromdate=$('.fromdate').val();
		    var todate=$('.todate').val();
		    var search=$('#cust_table_filter input[type=search]').val();		  
		    var export_url=baseurl+'?Fromdate='+fromdate+'&Todate='+todate+'&search='+search;		  
		    $("#exportlink").attr("href", export_url);
			$("#exportlink")[0].click();
			//window.open(export_url, '_blank');
		});
		  
	  }
	  
	  if ($('#exp_read_btn').length) 
	  {
		$('#exp_read_btn').click(function()
		{
			
			var baseurl=$('#base_url').val();
		    var fromdate=$('.fromdate').val();
		    var todate=$('.todate').val();
		    var search=$('#read_table_filter input[type=search]').val();
			var building=$("#build_search").val();
		    var export_url=baseurl+'?Fromdate='+fromdate+'&Todate='+todate+'&search='+search+'&Building='+building;		  
		    $("#exportlink").attr("href", export_url);
			$("#exportlink")[0].click();
			//window.open(export_url, '_blank');
		});
		  
	  }
	  
	  
	  function process(date)
	  {
		   var parts = date.split("/");
		   return new Date(parts[2], parts[1] - 1, parts[0]);
	  }

	});
	</script>
</body>

</html>