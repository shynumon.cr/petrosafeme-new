<?php  
		session_start();
	    $adminid=$_SESSION['adminid'];	  
	    if(!is_numeric($adminid)){header("Location: index.php");}
	    include('../utility/connection.php');
		include('include/header.php'); 
		
		$paymentsQuery = "SELECT * FROM payment ORDER BY pay_init_time DESC";
	
	    $resultPayments = mysqli_query($con,$paymentsQuery);
		
		$i=1;
	   
?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0)">Payments</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Payments</h4>
								<div class="text-right">
									<button type="button" id="exp_pay_btn" class="btn mb-1 btn-info">Export<span class="btn-icon-right"><i class="fa fa-file-excel-o"></i></span>
                                    </button>
								</div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="cust_table">
                                        <thead>
                                            <tr>
                                                <th>Sl.No</th>
                                                <th>Customer Name</th>
                                                <th>Amount</th>
												<th>Building</th>
												<th>Flat</th>
												<th>Order Ref</th>
                                                <th>Payment Reference</th>
                                                <th>Status</th>
												<th>Added Date</th>
                                            </tr>
                                        </thead>
									</table>
									
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
		
		<input type="hidden" id="base_url" value="https://petrosafeme.com/admin/export_payments.php"/>
		<a href="#"	id="exportlink" class="d-none" ></a>						
        <!--**********************************
            Content body end
        ***********************************-->
        
<?php  include('include/footer.php'); ?>        
        
    

    