<?php  
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=readings.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		session_start();
	    $adminid=$_SESSION['adminid'];	  
	    
	    include('../utility/connection.php');
		
		
		$readingsQuery = "SELECT * FROM meter_reading_updates ORDER BY upd_added_datetime DESC";
	
	    $resultReadings = mysqli_query($con,$readingsQuery);
		
		$i=1;
		
		
		
		
		
		
		
				## Read value
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		
		$from_date=isset($_GET['Fromdate'])?$_GET['Fromdate']:'';
		$to_date=isset($_GET['Todate'])?$_GET['Todate']:'';
		$fromdate = str_replace('/', '-', $from_date);
		$frmdate = date('Y-m-d', strtotime($fromdate));
		$enddate = str_replace('/', '-', $to_date);
		$todate = date('Y-m-d', strtotime($enddate));
		
		$rowperpage = $_POST['length']; // Rows display per page
		$columnIndex = $_POST['order'][0]['column']; // Column index
		$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
		$searchValue = (isset($_GET['Building'])&&$_GET['Building']!='')?$_GET['Building']:$_GET['search']; // Search value

		## Search 
		$searchQuery2 = " ";
		if($searchValue != ''){
		   
				
			$searchQuery2 = " and (upd_cust_id like '%".$searchValue."%' or 
				upd_cust_name like '%".$searchValue."%' or 
				upd_cust_email like '%".$searchValue."%' or 
				upd_cust_building_name like'%".$searchValue."%' or 
				upd_cust_flat_name like'%".$searchValue."%' or 
				upd_cust_mob like'%".$searchValue."%' or 
				upd_reading like'%".$searchValue."%' ) ";
		}
		
		$date_qry="";
		
		if($frmdate!='1970-01-01'&&$todate=='1970-01-01')
	    {
		   //console.log('start date only');
		   
		   $date_qry=" and DATE(upd_added_datetime)>='".$frmdate."' ";
	    }
	    else if($frmdate=='1970-01-01'&&$todate!='1970-01-01')
	    {
		   
		   $date_qry=" and DATE(upd_added_datetime)<='".$todate."' ";
		   
	    }
	    else if($frmdate!='1970-01-01'&&$todate!='1970-01-01')
	    {
		    if(strtotime($frmdate) <= strtotime($todate)){
			   $date_qry=" and DATE(upd_added_datetime)>='".$frmdate."' and DATE(upd_added_datetime)<='".$todate."' ";
			}
			
	    }

		## Total number of records without filtering
		$sel = mysqli_query($con,"select count(*) as allcount from meter_reading_updates");
		$records = mysqli_fetch_assoc($sel);
		$totalRecords = $records['allcount'];

		## Total number of record with filtering
		$sel = mysqli_query($con,"select count(*) as allcount from meter_reading_updates WHERE 1 ".$searchQuery2.$date_qry);
		
		$records = mysqli_fetch_assoc($sel);
		$totalRecordwithFilter = $records['allcount'];

		## Fetch records
		$empQuery = "select * from meter_reading_updates WHERE 1 ".$searchQuery2.$date_qry." ORDER BY upd_added_datetime DESC ";
		$empRecords = mysqli_query($con, $empQuery);
		$data = array();
		$i=($row)+1;?>
<style>
td,th{
  border: 1px solid black;
}

table {
  border-collapse: collapse;
}
</style>		
<table class="table table-striped table-bordered" id="cust_table">
	<thead>
		<tr>
			<th>Sl.No</th>
			<th>Name</th>
			<th>Date</th>
			<th>Flat</th>
			<th>Building</th>
			<th>Invoice Status</th>			
		</tr>
	</thead>
	<tbody>		
<?php 	while ($row = mysqli_fetch_assoc($empRecords)) {
			$image='';
		   if(strlen($row['upd_image'])>3)
		   {
			   $imgfulllink=str_replace("thumbs/","",$row['upd_image']);
			   $imgthumblink=$row['upd_image'];
			   $image='<a href="https://petrosafeme.com/'.$imgfulllink.'" data-fancybox><img src="https://petrosafeme.com/'.$imgthumblink.'" style="width:110px;height:auto;"/></a>';
		   }
		   $added_datetime=$row['upd_added_datetime'];
		   $added_date=date('d/m/Y',strtotime($added_datetime));
		   $added_time=date('g:i A',strtotime($added_datetime));
		   $datetime_text=$added_date."<br/>".$added_time;
		   $reading_text='';$action_text='';$name_text='';
		   $invoice_date = date('Y-m-d',strtotime($added_datetime));
		   $readid=$row['upd_id'];
		   $reading=$row['upd_reading'];
		   $custid=$row['upd_cust_id'];
		   if($row['upd_inv_create_stat']==0)
		    {
				$reading_text='<input type="number" value="'.$reading.'" class="read_edit readedit'.$readid.'"/>';
			    $action_text='Not Invoiced';
			}
		   else
			{
				$reading_text=$reading;
				$action_text='Invoiced';
				
			}
		    if($row['upd_cust_flat_name']){$name_text.=",<br/>Flat - ".$row['upd_cust_flat_name']."";}
			if($row['upd_cust_mob']){$name_text.=",<br/>Mob - ".$row['upd_cust_mob']."";}
			if($row['upd_cust_email']){$name_text.=",<br/>Email - ".$row['upd_cust_email']."";} ?>
		   
<tr>
	<td><?php echo $i;?></td>
	<td><?php echo $row['upd_cust_name'];?></td>
	<td><?php echo $added_date;?></td>
	<td><?php echo $row['upd_cust_flat_name'];?></td>
	<td><?php echo $row['upd_cust_building_name'];?></td>
	<td><?php echo $action_text;?></td>
</tr>
		   
<?php	   $i++;
		}

		
	   
?>

</tbody>
</table>