		<div class="footer">
            <div class="copyright">
                <div class="text-center"> Copyrights &amp; Powered by <a href="http://azinovatechnologies.com/" target="_blank"><span class="azinova-logo"></span><!--azinova-logo end--></a></div>
            </div>
        </div>
	</div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

<!--**********************************
        Scripts
    ***********************************-->
    <script src="js/custom.min.js"></script>
	<script src="js/settings.js"></script>
	<script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
	<script src="js/jquery.fancybox.js"></script>
	<script src="js/bootstrap-tagsinput.js"></script>
	<script src="js/select2.min.js"></script>
	
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!--<script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>-->
	<script src="./js/dashboard/dashboard-1.js"></script>
    <!-- Chartjs -->
    <script src="js/jquery.dataTables.min.js"></script>
    
    <script src="./plugins/tables/js/datatable-init/datatable-basic.min.js"></script>
	<script src="./plugins/summernote/dist/summernote.min.js"></script>
	<script src="js/dataTables.buttons.min.js"></script>
	<script src="js/jszip.min.js"></script>
	<script src="js/pdfmake.min.js"></script>
	<script src="js/vfs_fonts.js"></script>
	<script src="js/buttons.html5.min.js"></script>
    
	<script>
	var editor;
	$(document).ready(function(){
	  if ($('.select2').length) 
	  {
		  $('.select2').select2();
	  }
	  
	  if ($('.summernote').length) 
	  {
		  $('.summernote').summernote({height:450,minHeight:null,maxHeight:null,focus:!1});
	  }
		
	  if ($('#cust_table').length) 
	  {	
		   $('#cust_table').DataTable({
			  'processing': true,
			  'serverSide': true,
			  'bSort' : false,
			  'serverMethod': 'post',
			  'ajax': {
				  'url':'load_payments.php',
				  'data':{'cmd' : 'refresh','from':$('.fromdate').val(),'to':$('.todate').val()}
			  },
			  'columns': [
				 { data: 'sl_no' },
				 { data: 'cust_name' },
				 { data: 'amount' },
				 { data: 'building' },
				 { data: 'flat' },
				 { data: 'order' },
				 { data: 'reference' },
				 { data: 'status' },
				 { data: 'init_time' }
			  ],
			  dom: 'Bfrtip',
			  buttons: ['excelHtml5'],
			  exportOptions: {modifer: {page: 'all'}}
		   });
		   
		   var datefilter_html='<div class="pull-left">Start Date&nbsp;<input value="" class="dt_tbl_ip fromdate"/>&nbsp;&nbsp;End Date&nbsp;<input value="" class="dt_tbl_ip todate"/></div>';
		   $(datefilter_html).appendTo("#cust_table_wrapper .dataTables_filter"); //example is our table id
		   $( ".fromdate,.todate" ).datepicker({dateFormat: 'dd/mm/yy'});
	  }
	  
	  
	  if ($('#read_table').length) 
	  {	
		   var myTable=$('#read_table').DataTable({
			   responsive: {
								details: {
									type: 'column'
								}
							},
			  'processing': true,
			  'serverSide': true,
			  'bSort' : false,
			  'serverMethod': 'post',
			  'ajax': {
				  'url':'load_readings.php'
			  },
			  'columns': [
				 { data: 'sl_no' },
				 { data: 'upd_cust_id' },
				 { data: 'upd_cust_name' },
				 { data: 'upd_cust_building_name' },
				 { data: 'upd_added_datetime'},
				 { data: 'upd_image' },
				 { data: 'upd_reading' },
				 { data: 'action'}
			  ]
		   });
		   
		   var select_html='<?php echo $select_html;?>';
		   
		   $('#read_table').on( 'click', 'tbody td .create_inv', function () {
			   var read_id=$(this).data("readid");
			   var reading=$(".readedit"+read_id).val();
			   var cust_id=$(this).data("custid");
			   var invoicedate=$(this).data("invdate");
			   var user_id=1;
			   var params = new Object();
			   params.customer_id = cust_id;
			   params.meter_reading  = parseInt(reading);
			   params.user_id = 14;
			   var postarray=new Object();
			   postarray.params = params;
			   var jsonString= JSON.stringify(postarray);
			   //alert(jsonString);
		   
			      $(".invbtn"+read_id).html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>');
				  $(".invbtn"+read_id).attr("disabled", true);
				  $.ajax({
						url:"create_invoice.php",
						method:"POST",
						data:{'customer_id':cust_id,'meter_reading':parseInt(reading),'user_id':user_id,'update_id':read_id,'invoicedates':invoicedate},
						dataType: 'json',
						success:function(response) {
							var reading_id_stat=response.result.status;
							
							if(reading_id_stat=='success')
							{
								$(".invbtn"+read_id).html('Invoiced');	
								$(".invbtn"+read_id).removeClass("create_inv");								
								$(".invbtn"+read_id).addClass("invoiced");
								$(".readedit"+read_id).parent().html(reading);								
								
							}
							else
							{
								$(".invbtn"+read_id).html('Create Invoice');
								$(".invbtn"+read_id).attr("disabled", false);
								alert('Invoice creation error!Try again later.');
							}
								
					   },
					   error:function(response){
						    $(".invbtn"+read_id).html('Create Invoice');
							$(".invbtn"+read_id).attr("disabled", false);
							alert('Invoice creation error!Try again later.');
					   }

					  });
				 
			   
				
			} );
			
		   $(select_html).appendTo("#read_table_wrapper .dataTables_filter"); //example is our table id

			$(".dataTables_filter label").addClass("pull-right");
			
			$('.table-responsive').on( 'change', '#build_search', function () {
				var building=$(this).val();
				$("#read_table_filter input[type=search]").val(building);
				$("#read_table_filter input[type=search]").keyup();
				$("#read_table_filter input[type=search]").val('');
			});
			
			$("#read_table_filter input[type=search]").keydown(function(){
			  $("#build_search").val("");
			});

	  }	
	});
	</script>
</body>

</html>